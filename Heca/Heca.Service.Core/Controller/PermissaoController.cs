﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;

namespace Heca.Service.Core.Controller
{
    public class PermissaoController : ControllerBase<Permissao>, IPermissaoController
    {
        readonly IPermissaoApplication _service;

        public PermissaoController(IPermissaoApplication service) : base(service)
        {
            _service = service;
        }
    }
}
