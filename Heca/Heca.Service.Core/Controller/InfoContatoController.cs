﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;

namespace Heca.Service.Core.Controller
{
    public class InfoContatoController : ControllerBase<InfoContato>, IInfoContatoController
    {
        readonly IInfoContatoApplication _service;
        public InfoContatoController(IInfoContatoApplication service) : base(service)
        {
            _service = service;
        }
    }
}
