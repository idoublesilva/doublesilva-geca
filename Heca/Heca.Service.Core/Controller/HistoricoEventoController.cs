﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace Heca.Service.Core.Controller
{
    public class HistoricoEventoController : ControllerBase<HistoricoEvento>, IHistoricoEventoController
    {
        readonly IHistoricoEventoApplication _service;
        public HistoricoEventoController(IHistoricoEventoApplication service) : base(service)
        {
            _service = service;
        }

        [HttpGet]
        public HttpResponseMessage GetDataEventPoint(int idEvento)
        {
            var nroInscritoEvento = _service.Find(x => x.IdEvento == idEvento).ToList().GroupBy(x => new { x.ConfirmouInscricao })
                .Select(g => new { ConfirmouInscricao= g.Key.ConfirmouInscricao, NroInscrito=g.Count(), DtUltimaConfirm = g.Max(l => l.DtConfirmacao), DtUltimaInscricao = g.Max(l => l.DtIncricao)});

           return Request.CreateResponse<object>(nroInscritoEvento);
        }


        [HttpGet]
        public HttpResponseMessage GetParticipants(int idEvento)
        {
            var result = _service.Find(x => x.IdEvento == idEvento).ToList();
            
             var json = new { Todos= result, Confirmados = result.Where(x => x.ConfirmouInscricao),  Pendentes =  result.Where(x => !x.ConfirmouInscricao) };


            return Request.CreateResponse<object>(json);
        }

        [HttpPost]
        public HttpResponseMessage ConfirmEvent(string guid)
        {
            var confirm = _service.Find(x => x.ChaveTemporaria.ToString() == guid).First();
            confirm.ChaveTemporaria = Guid.Empty;
            confirm.DtConfirmacao = DateTime.Now;
            confirm.ConfirmouInscricao = true;

            return this.Update(confirm);

        }
    }
}
