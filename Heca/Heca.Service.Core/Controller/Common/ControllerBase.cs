﻿using Heca.Domain.Interfaces.Application.Common;
using Heca.Domain.Interfaces.Controller.Common;
using System.Collections.Generic;
using System.Web.Http;
using System.Net.Http;
using Heca.Domain.Validations;
using System.Net;

namespace Heca.Service.Core.Controller.Common
{

    public class ControllerBase<TEntity> : ApiController, IController<TEntity> where TEntity : class
    {
        readonly IApplication<TEntity> _application;   
        public ControllerBase(IApplication<TEntity> application)
        {
            _application = application;
         
        }

      
        [HttpPost]
        public virtual HttpResponseMessage Add(TEntity entity)
        {
            var result = _application.Add(entity);
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, result);
        }

        
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            var result = _application.Delete(id);
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, result);
        }

       [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var result = _application.Get(id);
            return Request.CreateResponse<TEntity>(HttpStatusCode.OK, result); 
        }

       
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            var result = _application.GetAll();
            return Request.CreateResponse<IEnumerable<TEntity>>(HttpStatusCode.OK, result);
        }

        
        [HttpPut]
        public HttpResponseMessage Update(TEntity entity)
        {
            var result = _application.Update(entity);
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, result);
        }
    }
}
