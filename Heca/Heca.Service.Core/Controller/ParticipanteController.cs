﻿using System.Net.Http;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;
using System;
using System.Linq;
using System.Net;
using Heca.Domain.Validations;
using System.Web.Http;

namespace Heca.Service.Core.Controller
{
    public class ParticipanteController : ControllerBase<Participante>, IParticipanteController
    {
        readonly IParticipanteApplication _service;
        readonly IUsuarioApplication _appUser;
        public ParticipanteController(IParticipanteApplication service, IUsuarioApplication appUser) : base(service)
        {
            _service = service;
            _appUser = appUser;
        }

        [HttpGet]
        public HttpResponseMessage ExistsUserName(string userName)
        {
            var result = _appUser.Find(x => x.Apelido.Trim().ToUpper() == userName.Trim().ToUpper()).FirstOrDefault();
            var validate = new ValidationResult();
            if (result != null)
            {
                validate.Add("Usuário já existe na base.");
            }
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, validate);
        }

        public HttpResponseMessage ReSendEmail(string guid)
        {
            var result = _service.ReSendEmail(guid);
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, result);            
        }

        public HttpResponseMessage ConfirmAccount(string guid)
        {
            var result = _service.ConfirmAccount(guid);
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, result);
        }

        public override HttpResponseMessage Add(Participante entity)
        {
            entity.ChaveTemporaria = Guid.NewGuid(); 
            return base.Add(entity);    
        }


    }
}
