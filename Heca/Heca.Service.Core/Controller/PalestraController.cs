﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;

namespace Heca.Service.Core.Controller
{
    public class PalestraController : ControllerBase<Palestra>, IPalestraController
    {
        readonly IPalestraApplication _service;
        public PalestraController(IPalestraApplication service) : base(service)
        {
            _service = service;
        }
    }
}
