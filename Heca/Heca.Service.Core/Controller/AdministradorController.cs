﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;
using System.Web.Http;
using System;
using System.Net.Http;
using System.Linq;
using Heca.Domain.Validations;
using System.Net;

namespace Heca.Service.Core.Controller
{

    [RoutePrefix("api/administradores")]
    public class AdministradorController : ControllerBase<Administrador>, IAdministradorController
    {
        readonly IAdministradorApplication _service;
        readonly IUsuarioApplication _appUser;
        public AdministradorController(IAdministradorApplication service, IUsuarioApplication appUser) : base(service)
        {
            _service = service;
            _appUser = appUser;
        }

        [HttpGet]
        public HttpResponseMessage ExistsUserName(string userName)
        {
            var result = _appUser.Find(x => x.Apelido.Trim().ToUpper() == userName.Trim().ToUpper()).FirstOrDefault();
            var validate = new ValidationResult();
            if (result != null)
            {
                validate.Add("Usuário já existe na base.");
            }
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, validate);
        }
    }
}
