﻿using System;
using System.Net.Http;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;
using Heca.Domain.Validations;
using System.Net;
using System.Web.Http;
using System.Linq;

namespace Heca.Service.Core.Controller
{
    public class ConfiguracaoController : ControllerBase<Configuracao>, IConfiguracaoController
    {
        readonly IConfiguracaoApplication _service;

        public ConfiguracaoController(IConfiguracaoApplication service) : base(service)
        {
            _service = service;
        }

        [HttpGet]
        public HttpResponseMessage ExistsSubDomain(string subDomain)
        {
            var result = _service.Find(x => x.TituloChave.Trim().ToUpper() == subDomain.Trim().ToUpper()).FirstOrDefault();
            var validate = new ValidationResult();

            if(result != null)
            {
                validate.Add("Sub dominio já existe.");
            }
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, validate);
        }
    }
}
