﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;

namespace Heca.Service.Core.Controller
{
    public class EnderecoController : ControllerBase<Endereco>, IEnderecoController
    {
        readonly IEnderecoApplication _service;
        public EnderecoController(IEnderecoApplication service) : base(service)
        {
            _service = service;
        }
    }
}
