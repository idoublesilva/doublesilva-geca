﻿using System;
using System.Net.Http;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;
using System.Linq;
using Heca.Domain.Validations;
using System.Net;
using System.Web.Http;

namespace Heca.Service.Core.Controller
{
    public class PromotorController : ControllerBase<Promotor>, IPromotorController
    {
        readonly IPromotorApplication _service;
        readonly IUsuarioApplication _appUser;
        public PromotorController(IPromotorApplication service, IUsuarioApplication appUser) : base(service)
        {
            _service = service;
            _appUser = appUser;
        }

        [HttpGet]
        public HttpResponseMessage ExistsUserName(string userName)
        {
           var result = _appUser.Find(x => x.Apelido.Trim().ToUpper() == userName.Trim().ToUpper()).FirstOrDefault();
            var validate = new ValidationResult();
            if(result != null)
            {
                validate.Add("Usuário já existe na base.");
            }
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, validate);
        }
    }
}
