﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;

namespace Heca.Service.Core.Controller
{
    public class PalestranteController : ControllerBase<Palestrante>, IPalestranteController
    {
        readonly IPalestranteApplication _service;

        public PalestranteController(IPalestranteApplication service) : base(service)
        {
            _service = service;
        }
    }
}
