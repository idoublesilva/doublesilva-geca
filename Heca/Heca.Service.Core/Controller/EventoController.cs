﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using Heca.Domain.Validations;

namespace Heca.Service.Core.Controller
{
    public class EventoController : ControllerBase<Evento>, IEventoController
    {
        readonly IEventoApplication _service;
        public EventoController(IEventoApplication service) : base(service)
        {
            _service = service;
        }

        [HttpGet]
        public HttpResponseMessage GetByConfiguracao(int idConfiguracao)
        {
            var result = _service.Find(x => x.IdConfiguracao == idConfiguracao).FirstOrDefault();
            return Request.CreateResponse<Evento>(HttpStatusCode.OK, result);
        }

        [HttpGet]
        public HttpResponseMessage GetByKeyTitle(string title)
        {
            var result = _service.Find(x => x.Configuracao.TituloChave == title).FirstOrDefault();
            return Request.CreateResponse<Evento>(HttpStatusCode.OK, result);
        }

        [HttpPut]
        public HttpResponseMessage PublishEvent(int id)
        {
            var result = _service.Find(x => x.IdEntity == id).FirstOrDefault();
            result.HabilitaEvento = true;
            return base.Update(result);;
        }


        [HttpPost]
        public HttpResponseMessage SendEmail(ContatoSite contato)
        {
            var result = _service.SendEmail(contato);
            return Request.CreateResponse<ValidationResult>(HttpStatusCode.OK, result);
        }

    }
}
