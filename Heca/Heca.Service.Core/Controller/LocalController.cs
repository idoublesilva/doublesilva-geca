﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller.Common;

namespace Heca.Service.Core.Controller
{
    public class LocalController : ControllerBase<Local>, ILocalController
    {
        readonly ILocalApplication _service;
        public LocalController(ILocalApplication service) : base(service)
        {
            _service = service;
        }
    }
}
