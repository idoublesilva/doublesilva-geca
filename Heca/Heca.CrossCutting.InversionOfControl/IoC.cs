﻿using CommonServiceLocator.NinjectAdapter.Unofficial;
using Heca.CrossCutting.InversionOfControl.Module;
using Microsoft.Practices.ServiceLocation;
using Ninject;

namespace Heca.CrossCutting.InversionOfControl
{
    public class IoC
    {
        public IKernel kernel;

        public IoC()
        {
            kernel = GetNinjectModules();
            ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(kernel));
        }

        private StandardKernel GetNinjectModules()
        {
            return new StandardKernel(
                             new ApplicationModule(),
                             new RepositoryModule(),
                             new ControllerModule(),
                             new ServiceModule()
                );
        }
    }
}
