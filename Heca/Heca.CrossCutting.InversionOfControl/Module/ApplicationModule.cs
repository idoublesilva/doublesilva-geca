﻿using Heca.Application;
using Heca.Domain.Interfaces.Application;
using Ninject.Modules;

namespace Heca.CrossCutting.InversionOfControl.Module
{
    public class ApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUsuarioApplication>().To<UsuarioApplication>();
            Bind<IAdministradorApplication>().To<AdministradorApplication>();
            Bind<IConfiguracaoApplication>().To<ConfiguracaoApplication>();
            Bind<IEnderecoApplication>().To<EnderecoApplication>();
            Bind<IEventoApplication>().To<EventoApplication>();
            Bind<IHistoricoEventoApplication>().To<HistoricoEventoApplication>();
            Bind<IInfoContatoApplication>().To<InfoContatoApplication>();
            Bind<ILocalApplication>().To<LocalApplication>();
            Bind<IPalestraApplication>().To<PalestraApplication>();
            Bind<IPalestranteApplication>().To<PalestranteApplication>();
            Bind<IParticipanteApplication>().To<ParticipanteApplication>();
            Bind<IPermissaoApplication>().To<PermissaoApplication>();
            Bind<IPromotorApplication>().To<PromotorApplication>();

        }
    }
}
