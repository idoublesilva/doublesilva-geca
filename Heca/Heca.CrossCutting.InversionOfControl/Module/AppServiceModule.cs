﻿using Heca.Domain.Interfaces.Controller;
using Heca.Service.Core.Controller;
using Ninject.Modules;

namespace Heca.CrossCutting.InversionOfControl.Module
{
    public class ControllerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAdministradorController>().To<AdministradorController>();
            Bind<IConfiguracaoController>().To<ConfiguracaoController>();
            Bind<IEnderecoController>().To<EnderecoController>();
            Bind<IEventoController>().To<EventoController>();
            Bind<IHistoricoEventoController>().To<HistoricoEventoController>();
            Bind<IInfoContatoController>().To<InfoContatoController>();
            Bind<ILocalController>().To<LocalController>();
            Bind<IPalestraController>().To<PalestraController>();
            Bind<IPalestranteController>().To<PalestranteController>();
            Bind<IParticipanteController>().To<ParticipanteController>();
            Bind<IPermissaoController>().To<PermissaoController>();
            Bind<IPromotorController>().To<PromotorController>();

        }
    }
}
