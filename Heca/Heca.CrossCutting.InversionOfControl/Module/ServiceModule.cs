﻿using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services;
using Ninject.Modules;

namespace Heca.CrossCutting.InversionOfControl.Module
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUsuarioService>().To<UsuarioService>();
            Bind<IAdministradorService>().To<AdministradorService>();
            Bind<IConfiguracaoService>().To<ConfiguracaoService>();
            Bind<IEnderecoService>().To<EnderecoService>();
            Bind<IEventoService>().To<EventoService>();
            Bind<IHistoricoEventoService>().To<HistoricoEventoService>();
            Bind<IInfoContatoService>().To<InfoContatoService>();
            Bind<ILocalService>().To<LocalService>();
            Bind<IPalestraService>().To<PalestraService>();
            Bind<IPalestranteService>().To<PalestranteService>();
            Bind<IParticipanteService>().To<ParticipanteService>();
            Bind<IPermissaoService>().To<PermissaoService>();
            Bind<IPromotorService>().To<PromotorService>();

        }
    }
}
