﻿using Heca.Domain.Interfaces.Repository;
using Heca.Infra.Data.Repository;
using Ninject.Modules;

namespace Heca.CrossCutting.InversionOfControl.Module
{
    public class RepositoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUsuarioRepository>().To<UsuarioRepository>();
            Bind<IAdministradorRepository>().To<AdministradorRepository>();
            Bind<IConfiguracaoRepository>().To<ConfiguracaoRepository>();
            Bind<IEnderecoRepository>().To<EnderecoRepository>();
            Bind<IEventoRepository>().To<EventoRepository>();
            Bind<IHistoricoEventoRepository>().To<HistoricoEventoRepository>();
            Bind<IInfoContatoRepository>().To<InfoContatoRepository>();
            Bind<ILocalRepository>().To<LocalRepository>();
            Bind<IPalestraRepository>().To<PalestraRepository>();
            Bind<IPalestranteRepository>().To<PalestranteRepository>();
            Bind<IParticipanteRepository>().To<ParticipanteRepository>();
            Bind<IPermissaoRepository>().To<PermissaoRepository>();
            Bind<IPromotorRepository>().To<PromotorRepository>();
            
        }
    }
}
