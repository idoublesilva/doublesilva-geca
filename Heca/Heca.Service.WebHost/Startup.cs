﻿using Heca.CrossCutting.InversionOfControl;
using Heca.Domain.Interfaces.Application;
using Heca.Service.Core.Configuration;
using Heca.Service.WebHost.Authorization;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace Heca.Service.WebHost
{
    
    public class Startup
    {
        private IKernel _kernel;
        public static HttpConfiguration HttpConfiguration { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            //Global.asax
            HttpConfiguration =  new HttpConfiguration();

            //var cors = new EnableCorsAttribute("*", "*", "*");
           // HttpConfiguration.EnableCors();
            
            AreaRegistration.RegisterAllAreas();
            //AutoMapperConfig.CreateMaps();//



            _kernel = CreateKernel();

            WebApiConfig.Register(HttpConfiguration);
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            //GlobalConfiguration.Configure(x => x.MapHttpAttributeRoutes());
            ConfigureOAuth(app);
           
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseNinjectMiddleware(() => _kernel).UseNinjectWebApi(HttpConfiguration);
            app.UseWebApi(HttpConfiguration);

            
           // GlobalConfiguration.Configuration.EnsureInitialized();
        }

        private static IKernel CreateKernel()
        {
            var ioc = new IoC();
            var kernel = ioc.kernel;
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);


                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/security/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(8),
                Provider = new AuthorizationServerProvider(_kernel.Get<IAdministradorApplication>(), _kernel.Get<IPromotorApplication>(), _kernel.Get<IParticipanteApplication>())
            };

            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}