﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace Heca.Service.WebHost.Authorization
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly IAdministradorApplication _admApp;
        private readonly IPromotorApplication _promotorApp;
        private readonly IParticipanteApplication _participanteApp;

        public AuthorizationServerProvider(IAdministradorApplication admApp, IPromotorApplication promotorApp,
                                           IParticipanteApplication participanteApp)
        {
            _admApp = admApp;
            _promotorApp = promotorApp;
            _participanteApp = participanteApp;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            await Task.Run(() => context.Validated());
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                var user = context.UserName;
                var password = context.Password;
                var adm = await Task.Run(() => _admApp.Find(x => x.Apelido.ToLower() == user.ToLower() && x.Senha == password).FirstOrDefault());
                var promotor = await Task.Run(() => _promotorApp.Find(x => x.Apelido.ToLower() == user.ToLower()  && x.Senha == password).FirstOrDefault());
                var participante = await Task.Run(() => _participanteApp.Find(x => x.Apelido.ToLower() == user.ToLower() && x.Senha == password).FirstOrDefault());

                if (adm == null && promotor == null && participante == null)
                {
                    context.SetError("404", "Usuário ou senha inválidos");
                    return;
                }

                var usuario = adm != null ? (Usuario)adm : promotor != null ? (Usuario)promotor : (Usuario)participante;

                bool isAdmin = adm != null;
                bool isPromotor = promotor != null;
                bool isParticipante = participante != null;

                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                        "username", usuario.Apelido
                    },
                    {
                        "idUser", usuario.IdEntity.ToString()
                    },
                    {
                       "name", usuario.Nome
                    },
                    {
                        "isAdmin", isAdmin.ToString().ToLower()
                    },
                    {
                        "isPromotor", isPromotor.ToString().ToLower()
                    },
                    {
                        "isParticipante", (isParticipante).ToString().ToLower()
                    },
                    {
                        "isActive", (isParticipante ? Guid.Empty ==  participante.ChaveTemporaria  : true).ToString().ToLower()
                    },                    
                    {
                        "guid", (isParticipante ? participante.ChaveTemporaria  : Guid.Empty).ToString()
                    }

                });


                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Name, usuario.Nome));
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, usuario.Email));
                identity.AddClaim(new Claim(ClaimTypes.PrimarySid, usuario.IdEntity.ToString()));

                var roles = new List<string>();

                //usuario.GrupoUsuario.AsParallel().ForAll(grupo => { roles.Add(grupo.Grupo.Nome); });
                //roles.Add(usuario.Grupo.Nome);
                GenericPrincipal principal = new GenericPrincipal(identity, roles.ToArray());
                var ticket = new AuthenticationTicket(identity, props);
                Thread.CurrentPrincipal = principal;
                context.Validated(ticket);

            }
            catch (Exception ex)
            {
                // _log.ErrorFormat("{0} - {1}", ex.Message, ex);
                context.SetError("500", "Falha ao autenticar");
            }
        }

        public override System.Threading.Tasks.Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return base.TokenEndpoint(context);
        }
    }
}