using System.Web.Mvc;
using System.Web.Routing;

namespace Heca.Service.WebHost.Areas.HelpPage
{
    public class HelpPageAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HelpPage";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
        "Help Area",
        "",
        new { controller = "Help", action = "Index" }
    ).DataTokens = new RouteValueDictionary(new { area = "HelpPage" });

            context.MapRoute(
                "HelpPage_Default",
                "Help/{action}/{apiId}",
                new { controller = "Help", action = "Index", apiId = UrlParameter.Optional });

            HelpPageConfig.Register(Startup.HttpConfiguration);
        }
    }
}