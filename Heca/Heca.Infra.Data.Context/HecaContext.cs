﻿using Heca.Domain.Entities;
using Heca.Infra.Data.Context.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Linq;

namespace Heca.Infra.Data.Context
{
    public class HecaContext : DbContext
    {


        public HecaContext()
            : base("DbHeca")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

          

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(255));

            modelBuilder.Properties<DateTime>()
               .Configure(p => p.HasColumnType("datetime2"));





            modelBuilder.Configurations.Add(new EnderecoMap());
            modelBuilder.Configurations.Add(new ConfiguracaoMap());
            
            modelBuilder.Configurations.Add(new HistoricoEventoMap());
            modelBuilder.Configurations.Add(new InfoContatoMap());
            modelBuilder.Configurations.Add(new LocalMap());
            modelBuilder.Configurations.Add(new PalestranteMap());
            modelBuilder.Configurations.Add(new PalestraMap());
            modelBuilder.Configurations.Add(new PermissaoMap());
            modelBuilder.Configurations.Add(new PalestraInscritoMap());
            modelBuilder.Configurations.Add(new UsuarioMap());
            modelBuilder.Configurations.Add(new AdministradorMap());
            modelBuilder.Configurations.Add(new PromotorMap());
           modelBuilder.Configurations.Add(new ParticipanteMap());
            modelBuilder.Configurations.Add(new EventoMap());

            base.OnModelCreating(modelBuilder);


        }

        public override int SaveChanges()
        {

            ChangeTracker.Entries().ToList().ForEach(item =>
            {
                var entidade = item.Entity as BaseEntity;

                if (item.State == EntityState.Added)
                {
                    entidade.DtCreated = DateTime.Now;
                    entidade.DtUpdated = DateTime.Now;
                    entidade.IsActive = true;                    
                }

                if (item.State == EntityState.Modified)
                {
                    item.Property("DtCreated").IsModified = false;
                    entidade.DtUpdated = DateTime.Now;
                }

                if (item.State == EntityState.Deleted)
                {
                    SoftDelete(item);
                }
            });

            try
            {
                return base.SaveChanges();
            }
            catch (Exception)
            {
                return 0;
            }

        }

        //public DbSet<Administrador> Administradores { get; set; }
        public DbSet<Configuracao> Configuracoes { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<Evento> Eventos { get; set; }
        public DbSet<HistoricoEvento> HistoricoEventos { get; set; }
        public DbSet<InfoContato> InfoContatos { get; set; }
        public DbSet<Local> Locais { get; set; }
        public DbSet<Palestrante> Palestrantes { get; set; }
        public DbSet<Palestra> Palestras { get; set; }
        //public DbSet<Participante> Participantes { get; set; }
        public DbSet<Permissao> Permissoes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }

        private static Dictionary<Type, EntitySetBase> _mappingCache = new Dictionary<Type, EntitySetBase>();

        private string GetTableName(Type type)
        {
            EntitySetBase es = GetEntitySet(type);

            return string.Format("[{0}].[{1}]",
                es.MetadataProperties["Schema"].Value,
                es.MetadataProperties["Table"].Value);
        }

        private string GetPrimaryKeyName(Type type)
        {
            EntitySetBase es = GetEntitySet(type);

            return es.ElementType.KeyMembers[0].Name;
        }

        private EntitySetBase GetEntitySet(Type type)
        {
            if (!_mappingCache.ContainsKey(type))
            {
                ObjectContext octx = ((IObjectContextAdapter)this).ObjectContext;

                string typeName = ObjectContext.GetObjectType(type).Name;

                var es = octx.MetadataWorkspace
                                .GetItemCollection(DataSpace.SSpace)
                                .GetItems<EntityContainer>()
                                .SelectMany(c => c.BaseEntitySets
                                                .Where(e => e.Name == typeName))
                                .FirstOrDefault();

                if (es == null)
                    throw new ArgumentException("Entity type not found in GetTableName", typeName);

                _mappingCache.Add(type, es);
            }

            return _mappingCache[type];
        }

        private void SoftDelete(DbEntityEntry entry)
        {
            lock (Database)
            {
                Type entryEntityType = entry.Entity.GetType();

                string tableName = GetTableName(entryEntityType);
                string primaryKeyName = GetPrimaryKeyName(entryEntityType);
                

                string sqlCmd = string.Format("UPDATE {0} SET IsDeleted = 1, [DtUpdated] = GetDate() WHERE [{1}] = {2} ", tableName, primaryKeyName, entry.OriginalValues[primaryKeyName]);
              //  var sqlCmd = sql + (entry.Entity as BaseEntity).IdEntity;

                Database.ExecuteSqlCommand(sqlCmd);

                entry.State = EntityState.Detached;
            }
        }

    }
}
