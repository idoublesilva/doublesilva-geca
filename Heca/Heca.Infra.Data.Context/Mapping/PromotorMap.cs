﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class PromotorMap : EntityTypeConfiguration<Promotor>
    {

        public PromotorMap()
        {
            
            //HasKey(x => x.IdEntity);
            ////Ignore(p => p.IsDeleted);
            //Map(p =>
            //{
            //    p.ToTable("Promotores");
            //  //  p.Requires("IsDeleted").HasValue(false);
            //});

            HasRequired(x => x.Permissao)
                .WithMany()
                .HasForeignKey(x => x.IdPermissao);

            HasRequired(x => x.ConfiguracaoEvento)
                .WithMany(x => x.Promotores)
                .HasForeignKey(x => x.IdConfiguracao);
            
        }
    }
}
