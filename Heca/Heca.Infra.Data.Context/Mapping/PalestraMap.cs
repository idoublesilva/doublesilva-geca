﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class PalestraMap : EntityTypeConfiguration<Palestra>
    {

        public PalestraMap()
        {
            
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {
                p.ToTable("Palestras");
                p.Requires("IsDeleted").HasValue(false);
            });

            HasRequired(x => x.Local)
                .WithMany()
                .HasForeignKey(x => x.IdLocal);

            HasRequired(x => x.Palestrante)
                .WithMany()
                .HasForeignKey(x => x.IdPalestrante);

            HasRequired(x => x.Evento)
                .WithMany(c => c.Palestras)
                .HasForeignKey(x => x.IdEvento);
            
       
        }
    }
}
