﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class UsuarioMap : EntityTypeConfiguration<Usuario>
    {

        public UsuarioMap()
        {
            
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {
                p.ToTable("Usuarios");
                p.Requires("IsDeleted").HasValue(false);

            }).Map<Administrador>(m => m.Requires("UserType").HasValue(1))
            .Map<Participante>(m => m.Requires("UserType").HasValue(2))
            .Map<Promotor>(m => m.Requires("UserType").HasValue(3)); 
           
        }


    }
}
