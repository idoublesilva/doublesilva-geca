﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class InfoContatoMap : EntityTypeConfiguration<InfoContato>
    {

        public InfoContatoMap()
        {
            
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {
                p.ToTable("InfoContatos");
                p.Requires("IsDeleted").HasValue(false);
            });
        }
    }
}
