﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class LocalMap : EntityTypeConfiguration<Local>
    {

        public LocalMap()
        {
            
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {
                p.ToTable("Locais");
                p.Requires("IsDeleted").HasValue(false);
            });

           
        }
    }
}
