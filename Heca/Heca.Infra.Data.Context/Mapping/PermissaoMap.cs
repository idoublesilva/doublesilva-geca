﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class PermissaoMap : EntityTypeConfiguration<Permissao>
    {

        public PermissaoMap()
        {
            
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {
                p.ToTable("Permissoes");
                p.Requires("IsDeleted").HasValue(false);
            });
        }
    }
}
