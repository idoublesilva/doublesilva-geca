﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class HistoricoEventoMap : EntityTypeConfiguration<HistoricoEvento>
    {

        public HistoricoEventoMap()
        {
            
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {
                p.ToTable("HistoricoEventos");
                p.Requires("IsDeleted").HasValue(false);
            });

            HasRequired(x => x.Evento)
                .WithMany()
                .HasForeignKey(x => x.IdEvento);

            HasRequired(x => x.Participante)
                .WithMany(p => p.HistoricoEventos)
                .HasForeignKey(x => x.IdParcipante);

         
        }
    }
}
