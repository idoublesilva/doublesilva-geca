﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class EventoMap : EntityTypeConfiguration<Evento>
    {

        public EventoMap()
        {
            
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            { 
                p.ToTable("Eventos");
                p.Requires("IsDeleted").HasValue(false);
            });

            
            HasRequired(x => x.Contato)
                .WithMany()
                .HasForeignKey(x => x.IdInfoContato);

            HasRequired(x => x.Endereco)
                 .WithMany()
                 .HasForeignKey(x => x.IdEndereco);

            HasRequired(x => x.Configuracao)
                .WithMany()
                .HasForeignKey(x => x.IdConfiguracao);

            Property(x => x.Descricao)
            .HasColumnType("text");

        }
    }
}
