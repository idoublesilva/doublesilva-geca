﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class BaseMapping<T> : EntityTypeConfiguration<T> where T : BaseEntity
    {
        public BaseMapping()
        {
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {
                p.Requires("IsDeleted").HasValue(false);
            });
        }
    }
}
