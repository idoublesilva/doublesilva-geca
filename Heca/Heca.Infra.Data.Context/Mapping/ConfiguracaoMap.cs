﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class ConfiguracaoMap : EntityTypeConfiguration<Configuracao>
    {

        public ConfiguracaoMap() : base()
        {
            //ToTable("Configuracoes");
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {              
                p.ToTable("Configuracoes");
                p.Requires("IsDeleted").HasValue(false);
            });

          
            
        }
    }
}
