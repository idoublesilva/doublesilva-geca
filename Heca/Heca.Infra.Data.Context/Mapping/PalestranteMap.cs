﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class PalestranteMap : EntityTypeConfiguration<Palestrante>
    {

        public PalestranteMap()
        {
            
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);
            Map(p =>
            {
                p.ToTable("Palestrantes");
                p.Requires("IsDeleted").HasValue(false);
            });
        }
    }
}
