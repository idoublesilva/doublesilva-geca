﻿using Heca.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Heca.Infra.Data.Context.Mapping
{
    public class PalestraInscritoMap : EntityTypeConfiguration<PalestraIncrito>
    {
        public PalestraInscritoMap()
        {
            Map(p =>
            {
                p.ToTable("PalestrasInscrito");
            });
            HasKey(x => x.IdEntity);
            Ignore(p => p.IsDeleted);

            HasRequired(x => x.Palestra)
                .WithMany()
                .HasForeignKey(x => x.IdPalestra);
            HasRequired(x => x.HistoricoEvento)
                .WithMany(c => c.PalestrasInscrito)
                .HasForeignKey(x => x.IdHistoricoEvento);
           
        }
    }
}
