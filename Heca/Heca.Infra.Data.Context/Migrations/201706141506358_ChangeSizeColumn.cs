namespace Heca.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSizeColumn : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Eventos", "Descricao", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Eventos", "Descricao", c => c.String(maxLength: 255, unicode: false));
        }
    }
}
