namespace Heca.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSizeColumn_4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Eventos", "Descricao", c => c.String(unicode: false, storeType: "text"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Eventos", "Descricao", c => c.String(maxLength: 255, unicode: false));
        }
    }
}
