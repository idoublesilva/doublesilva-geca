namespace Heca.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBairroContato : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Enderecos", "Bairro", c => c.String(maxLength: 255, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Enderecos", "Bairro");
        }
    }
}
