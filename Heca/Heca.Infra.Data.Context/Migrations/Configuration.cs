namespace Heca.Infra.Data.Context.Migrations
{
    using Domain.Entities;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Heca.Infra.Data.Context.HecaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Heca.Infra.Data.Context.HecaContext context)
        {
            /*
            context.Usuarios.AddOrUpdate
                (a => a.Apelido,
                new Administrador() { Apelido = "Admin", Email = "twos@gmail.com", Senha = "102030", Nome = "Administrador" });
            var config = new Configuracao()
            {
                TituloChave = "Teste-Evento",
                ConfirmaEmailInscricao = true,
                CriarPalestra = true,
                DtInicioInscricao = DateTime.Now.AddMonths(2),
                DtFimInscricao = DateTime.Now.AddMonths(3),
                NroMaximoInscrito = 100,

            };

            context.Configuracoes.AddOrUpdate(x =>
           x.TituloChave, config

                         );


            var promotor01 = new Promotor()
            {
                Apelido = "Promotor01",
                Senha = "102030",
                Email = "promotor01@teste.com",
                Permissao = new Permissao() { ConfirmaPresenca = true, GeraCertificado = true, HabilitaInscricao = true, PublicaEvento = true, EditaEvento = true },
                Nome = "Sr. Promotor 01",
                IdConfiguracao = config.IdEntity
            };


            context.Usuarios.AddOrUpdate(p => p.Apelido, promotor01);


            var evento = new Evento()
            {
                Contato = new InfoContato() { Email = "contato@teste.com", NomeResponsavel = "Responsavel 01" },
                HabilitaEvento = true,
                HabilitaInscricao = false,
                Descricao = "Teste para o evento Heca",
                Titulo = "Evento HECA",
                Endereco = new Endereco { Logradouro = "Rua Y", Cidade = "Poa", Estado = "RS", Numero = 571 },
                IdConfiguracao = config.IdEntity
            };

            context.Eventos.AddOrUpdate(
             e => e.IdConfiguracao,
             evento);

            var participante = new Participante()
            {
                Apelido = "Participane01",
                Email = "participante@ssevento01.com",
                Nome = "Participante 01",
                ChaveTemporaria = Guid.NewGuid(),
                Senha = "102030"

            };

            
            

            context.Usuarios.Attach(participante);
            //context.Eventos.Attach(evento);

            var historico = new HistoricoEvento()
            {
                IdEvento = 1,
                IdParcipante = 3,
                DtIncricao = DateTime.Now,
                DtConfirmacao = DateTime.Now,
                ConfirmouInscricao = true
            };

           // participante.HistoricoEventos.Add(historico);
            //context.Usuarios.AddOrUpdate(x=> x.Apelido, participante);
            
            context.HistoricoEventos.AddOrUpdate(x => x.IdEvento, historico);
            context.SaveChanges();
            //context.HistoricoEventos.AddOrUpdate(h => h.IdParcipante, historico);


            //Palestra palestra = new Palestra();
            //palestra.IdEvento = evento.IdEntity;
            //palestra.Titulo = "Palestra 01";
            //palestra.Tema = "Tema Palestra 01";
            //palestra.Local = new Local()
            //{
            //    Nome = "Local Palestra 01",
            //    LotacaoMaxima = 300
            //};
            //palestra.Descricao = "Descri��o Palestra 01";
            //palestra.DtPalestra = new DateTime(2017, 11, 28);
            //palestra.HrInicio = new TimeSpan(09, 00, 00);
            //palestra.HrFim = new TimeSpan(11, 00, 00);
            //palestra.Palestrante = new Palestrante()
            //{
            //    Nome = "Palestrante 01",
            //    Descicao = "Palestrante Description"

            //};





            //context.Palestras.AddOrUpdate(
            //    p => p.IdEvento, palestra);


            // context.SaveChanges();*/
        }
    }
}
