namespace Heca.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChaveTemp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HistoricoEventos", "ChaveTemporaria", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HistoricoEventos", "ChaveTemporaria");
        }
    }
}
