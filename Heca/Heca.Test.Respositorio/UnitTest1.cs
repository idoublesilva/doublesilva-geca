﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Heca.Infra.Data.Repository;
using System.Collections.Generic;
using System.Linq;
using Heca.Domain.Entities;

namespace Heca.Test.Respositorio
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void AddHistoricoEvento()
        {
            ParticipanteRepository partRepo = new ParticipanteRepository();
            var participante = partRepo.GetAll().FirstOrDefault();

            EventoRepository evenRepo = new EventoRepository();
            var evento = evenRepo.GetAll().FirstOrDefault();

            var historico = new HistoricoEvento()
            {
                IdEvento = evento.IdEntity,
                IdParcipante = participante.IdEntity,
                DtIncricao = DateTime.Now,
                ConfirmouInscricao = true,
                DtConfirmacao = DateTime.Now
            };

            var historicoRepo = new HistoricoEventoRepository();
            if(historicoRepo.Find(x => x.IdEvento ==evento.IdEntity && x.IdParcipante == participante.IdEntity).FirstOrDefault() != null)
            historicoRepo.Add(historico);
            

        }

        [TestMethod]
        public void AddPalestra()
        {
            EventoRepository evenRepo = new EventoRepository();
            var evento = evenRepo.GetAll().FirstOrDefault();

            Palestra palestra = new Palestra();
            palestra.IdEvento = evento.IdEntity;
            palestra.Titulo = "Palestra 01";
            palestra.Tema = "Tema Palestra 01";
            palestra.Local = new Local()
            {
                Nome = "Local Palestra 01",
                LotacaoMaxima = 300
            };
            palestra.Descricao = "Descrição Palestra 01";
            palestra.DtPalestra = new DateTime(2017, 11, 28);
            palestra.HrInicio = new TimeSpan(09, 00, 00);
            palestra.HrFim = new TimeSpan(11, 00, 00);
            palestra.Palestrante = new Palestrante()
            {
                Nome = "Palestrante 01",
                Descricao = "Palestrante Description"

            };

            var palestraRepo = new PalestraRepository();
            palestraRepo.Add(palestra);


            
        }
    }
}
