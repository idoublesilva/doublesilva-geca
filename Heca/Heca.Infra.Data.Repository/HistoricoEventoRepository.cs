﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class HistoricoEventoRepository : RepositoryBase<HistoricoEvento>, IHistoricoEventoRepository
    {
        public override void Delete(HistoricoEvento entity)
        {           
            foreach (var item in entity.PalestrasInscrito)
            {
                Context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }
            entity.Evento = null;
            entity.Participante = null;
            base.Delete(entity);
        }

        public override void Update(HistoricoEvento entity)
        {
            foreach (var item in entity.PalestrasInscrito)
            {
                Context.Entry(item).State = item.IdEntity > 0 ? System.Data.Entity.EntityState.Modified : System.Data.Entity.EntityState.Added;
            }
            entity.Evento = null;
            entity.Participante = null;
            base.Update(entity);

        }
    }
}
