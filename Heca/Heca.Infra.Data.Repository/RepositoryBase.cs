﻿using Heca.Domain.Interfaces.Repository.Common;
using Heca.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Heca.Infra.Data.Repository
{
    public class RepositoryBase<TEntity> : IRepository<TEntity>, IDisposable where TEntity : class
    {
        public readonly HecaContext _dbContext;

        private readonly IDbSet<TEntity> _dbSet;

        public RepositoryBase()
        {

            _dbContext = new HecaContext();
            _dbSet = _dbContext.Set<TEntity>();
        }

        protected HecaContext Context { get { return _dbContext; } }

        protected IDbSet<TEntity> DbSet { get { return _dbSet; } }

        public virtual void Add(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Added;
            Context.SaveChanges();
        }

        public virtual void Delete(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Deleted;
            Context.SaveChanges();
        }

        public virtual TEntity Get(int id)
        {
            var entity = DbSet.Find(id);
            return entity;
        }

        public virtual void Update(TEntity entity)
        {
            //var entry = Context.Entry(entity);
            //DbSet.Attach(entity);
            //entry.State = EntityState.Modified;
            
           Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }

        public virtual IEnumerable<TEntity> GetAll(bool @readonly = false)
        {
            return @readonly ? DbSet.AsNoTracking().ToList() : DbSet.ToList();
        }

        public virtual IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, bool @readonly = false)
        {
            return @readonly ? DbSet.Where(predicate).AsNoTracking() : DbSet.Where(predicate);
        }

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;

            if (Context == null) return;
            Context.Dispose();
        }

        #endregion

    }
}
