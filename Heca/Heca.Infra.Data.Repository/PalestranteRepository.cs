﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class PalestranteRepository : RepositoryBase<Palestrante>, IPalestranteRepository
    {
        public override void Delete(Palestrante entity)
        {
            base.Delete(entity);
        }
    }
}
