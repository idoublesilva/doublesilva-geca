﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class ConfiguracaoRepository : RepositoryBase<Configuracao>, IConfiguracaoRepository
    {
        public override void Delete(Configuracao entity)
        {
            base.Delete(entity);
        }

        public override void Add(Configuracao entity)
        {
            foreach (var item in entity.Promotores)
            {
                Context.Entry(item).State = Context.Entry(item.Permissao).State = System.Data.Entity.EntityState.Added;
                 
            }
            base.Add(entity);
        }

        public override void Update(Configuracao entity)
        {

            foreach (var item in entity.Promotores)
            {
                Context.Entry(item).State = item.IdEntity == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                Context.Entry(item.Permissao).State = item.IdEntity == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                
            }

            base.Update(entity);    
        }
    }
}
