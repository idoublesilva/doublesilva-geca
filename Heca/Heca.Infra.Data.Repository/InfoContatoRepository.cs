﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class InfoContatoRepository : RepositoryBase<InfoContato>, IInfoContatoRepository
    {
        public override void Delete(InfoContato entity)
        {
            base.Delete(entity);
        }
    }
}
