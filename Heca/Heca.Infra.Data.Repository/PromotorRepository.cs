﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Heca.Infra.Data.Repository
{
    public class PromotorRepository : UsuarioRepository, IPromotorRepository
    {
        
        public void Add(Promotor entity)
        {
            DbSet.Add(entity);
            Context.SaveChanges();
        }

        public virtual void Delete(Promotor entity)
        {
            DbSet.Remove(entity);
            Context.SaveChanges();
        }

        public virtual Promotor Get(int id)
        {
            var entity = DbSet.OfType<Promotor>().FirstOrDefault(x => x.IdEntity == id);
            return entity;
        }

        public virtual void Update(Promotor entity)
        {
            var entry = _dbContext.Entry(entity);
            DbSet.Attach(entity);
            entry.State = EntityState.Modified;
            Context.SaveChanges();
            //Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual IEnumerable<Promotor> GetAll(bool @readonly = false)
        {
            return @readonly ? DbSet.AsNoTracking().OfType<Promotor>().ToList() : DbSet.OfType<Promotor>().ToList();
        }

        public virtual IEnumerable<Promotor> Find(Expression<Func<Promotor, bool>> predicate, bool @readonly = false)
        {
            return @readonly ? DbSet.OfType<Promotor>().Where(predicate).AsNoTracking() : DbSet.OfType<Promotor>().Where(predicate);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
