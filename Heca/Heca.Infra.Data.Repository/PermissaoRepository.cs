﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class PermissaoRepository : RepositoryBase<Permissao>, IPermissaoRepository
    {
        public override void Delete(Permissao entity)
        {
            base.Delete(entity);
        }
    }
}
