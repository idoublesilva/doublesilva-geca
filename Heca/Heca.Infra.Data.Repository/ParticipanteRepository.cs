﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Heca.Infra.Data.Repository
{
    public class ParticipanteRepository : UsuarioRepository,IParticipanteRepository
    {
        
        

        public virtual void Add(Participante entity)
        {
            DbSet.Add(entity);
            Context.SaveChanges();
        }

        public virtual void Delete(Participante entity)
        {
            DbSet.Remove(entity);
            Context.SaveChanges();
        }

        public virtual Participante Get(int id)
        {
            var entity = DbSet.OfType<Participante>().FirstOrDefault(x => x.IdEntity == id);
            return entity;
        }

        public virtual void Update(Participante entity)
        {
            Context.Entry(entity).State = EntityState.Modified;

            foreach (var item in entity.HistoricoEventos)
            {
                Context.Entry(item).State = EntityState.Modified;
            }

           // DbSet.Attach(entity);
            //entry
            Context.SaveChanges();
            //Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual IEnumerable<Participante > GetAll(bool @readonly = false)
        {
            return @readonly ? DbSet.AsNoTracking().OfType<Participante>().ToList() : DbSet.OfType<Participante>().ToList();
        }

        public virtual IEnumerable<Participante> Find(Expression<Func<Participante, bool>> predicate, bool @readonly = false)
        {
            return @readonly ? DbSet.OfType<Participante>().Where(predicate).AsNoTracking() : DbSet.OfType<Participante>().Where(predicate);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
