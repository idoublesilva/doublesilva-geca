﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class EnderecoRepository : RepositoryBase<Endereco>, IEnderecoRepository
    {
        public override void Delete(Endereco entity)
        {
            base.Delete(entity);
        }
    }
}
