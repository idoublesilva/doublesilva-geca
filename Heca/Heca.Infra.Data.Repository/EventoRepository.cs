﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class EventoRepository : RepositoryBase<Evento>, IEventoRepository
    {
        public override void Delete(Evento entity)
        {
            base.Delete(entity);
        }

        public override void Add(Evento entity)
        {
            Context.Entry(entity.Contato).State = System.Data.Entity.EntityState.Added;
            Context.Entry(entity.Endereco).State = System.Data.Entity.EntityState.Added;
            var teste = entity.Descricao;
            entity.Descricao = "Teste";
            base.Add(entity);
            if(entity.IdEntity > 0)
            {
                Context.Database.ExecuteSqlCommand(string.Format("Update Eventos   SET Descricao = '{1}' where [IdEntity] = {0} ", entity.IdEntity, teste));
            }
        }

        public override void Update(Evento entity)
        {
            

            entity.Configuracao = null;
            foreach (var palestra in entity.Palestras)
            {
                Context.Entry(palestra).State = palestra.IdEntity > 0 ? System.Data.Entity.EntityState.Modified : System.Data.Entity.EntityState.Added;
                Context.Entry(palestra.Local).State = palestra.Local.IdEntity > 0 ? System.Data.Entity.EntityState.Modified : System.Data.Entity.EntityState.Added;
                Context.Entry(palestra.Palestrante).State = palestra.Palestrante.IdEntity > 0 ? System.Data.Entity.EntityState.Modified : System.Data.Entity.EntityState.Added;
            }
            Context.Entry(entity.Endereco).State = System.Data.Entity.EntityState.Modified;
            Context.Entry(entity.Contato).State = System.Data.Entity.EntityState.Modified;

           // entity.Descricao = System.Net.WebUtility.HtmlEncode(entity.Descricao);

            Context.Database.ExecuteSqlCommand(string.Format("Update Eventos   SET Descricao = '{1}' where [IdEntity] = {0} ", entity.IdEntity, entity.Descricao));

            // DbSet.Attach(entity);
            Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
            //base.Update(entity);
        }
    }
}
