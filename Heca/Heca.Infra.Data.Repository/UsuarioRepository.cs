﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Heca.Infra.Data.Repository
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        public readonly HecaContext _dbContext;

        private readonly IDbSet<Usuario> _dbSet;

        public UsuarioRepository()
        {

            _dbContext = new HecaContext();
            _dbSet = _dbContext.Set<Usuario>();
        }

        
    }
}
