﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class PalestraRepository : RepositoryBase<Palestra>, IPalestraRepository
    {
        public override void Delete(Palestra entity)
        {
            base.Delete(entity);
        }

        public override void Update(Palestra entity)
        {
            Context.Entry(entity.Palestrante).State = System.Data.Entity.EntityState.Modified;
            Context.Entry(entity.Local).State = System.Data.Entity.EntityState.Modified;
            base.Update(entity);
        }

        public override void Add(Palestra entity)
        {
            Context.Entry(entity.Palestrante).State = System.Data.Entity.EntityState.Added;
            Context.Entry(entity.Local).State = System.Data.Entity.EntityState.Added;
            base.Add(entity);
        }
    }
}
