﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Infra.Data.Repository
{
    public class LocalRepository : RepositoryBase<Local>, ILocalRepository
    {
        public override void Delete(Local entity)
        {
            base.Delete(entity);
        }
    }
}
