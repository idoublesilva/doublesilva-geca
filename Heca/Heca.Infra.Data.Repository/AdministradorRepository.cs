﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Heca.Infra.Data.Repository
{
    public class AdministradorRepository : UsuarioRepository, IAdministradorRepository
    {
        public virtual void Add(Administrador entity)
        {
            DbSet.Add(entity);
            Context.SaveChanges();
        }

        public virtual void Delete(Administrador entity)
        {
            DbSet.Remove(entity);
            Context.SaveChanges();
        }

        public virtual Administrador Get(int id)
        {
            var entity = DbSet.OfType<Administrador>().FirstOrDefault(x => x.IdEntity == id);
            return entity;
        }

        public virtual void Update(Administrador entity)
        {
            var entry = _dbContext.Entry(entity);
            DbSet.Attach(entity);
            entry.State = EntityState.Modified;
            Context.SaveChanges();
            //Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual IEnumerable<Administrador> GetAll(bool @readonly = false)
        {
            return @readonly ? DbSet.AsNoTracking().OfType<Administrador>().ToList() : DbSet.OfType<Administrador>().ToList();
        }

        public virtual IEnumerable<Administrador> Find(Expression<Func<Administrador, bool>> predicate, bool @readonly = false)
        {
            return @readonly ? DbSet.OfType<Administrador>().Where(predicate).AsNoTracking() : DbSet.OfType<Administrador>().Where(predicate);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
