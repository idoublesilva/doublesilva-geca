﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class LocalService : ServiceBase<Local>, ILocalService
    {
        readonly ILocalRepository _repo;
        public LocalService(ILocalRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
