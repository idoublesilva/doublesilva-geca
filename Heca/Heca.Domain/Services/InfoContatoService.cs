﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class InfoContatoService : ServiceBase<InfoContato>, IInfoContatoService
    {
        readonly IInfoContatoRepository _repo;
        public InfoContatoService(IInfoContatoRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
