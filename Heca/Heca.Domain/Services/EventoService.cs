﻿using System;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;
using Heca.Domain.Validations;
using System.IO;
using System.Configuration;
using System.Linq;
using Heca.CrossCutting.Util;
using System.Threading.Tasks;

namespace Heca.Domain.Services
{
    public class EventoService : ServiceBase<Evento>, IEventoService
    {
        readonly IEventoRepository _repo;
        public EventoService(IEventoRepository repo) : base(repo)
        {
            _repo = repo;
        }

        public ValidationResult SendEmail(ContatoSite contato)
        {
            var evento = base.Find(x => x.IdEntity == contato.IdEvento).FirstOrDefault();

            try
            {
               
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template/ContatoSite.html");

                // Open the file to read from.
                string readText = File.ReadAllText(path);
                readText = readText.Replace("#NOME_RESPONSAVEL#", evento.Contato.NomeResponsavel);
                readText = readText.Replace("#NOME_CONTATO#", contato.Nome.ToUpper());
                readText = readText.Replace("#EMAIL_CONTATO#", contato.Email.ToLower());
                readText = readText.Replace("#ASSUNTO_CONTATO#", contato.Assunto);
                readText = readText.Replace("#MENSAGEM_CONTATO#", contato.Mensagem);

                MailService mail = new MailService();
                Task.Run(() => mail.SendEmail(evento.Contato.Email, string.Format("Contato do Site {0} - Geca", evento.Titulo), readText)).Wait();
            }
            catch (Exception ex)
            {
                _validation.Model = evento.Contato;
                _validation.Add(ex.Message);
                _validation.Add(ex.StackTrace);
            }

            return _validation;
        }
    }
}
