﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class PermissaoService : ServiceBase<Permissao>, IPermissaoService
    {
        readonly IPermissaoRepository _repo;
        public PermissaoService(IPermissaoRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
