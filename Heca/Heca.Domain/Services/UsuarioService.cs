﻿using Heca.Domain.Interfaces.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heca.Domain.Entities;
using System.Linq.Expressions;
using Heca.Domain.Interfaces.Repository;

namespace Heca.Domain.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _repo;
        public UsuarioService(IUsuarioRepository repo)
        {
            _repo = repo;
        }
        public IEnumerable<Usuario> Find(Expression<Func<Usuario, bool>> predicate, bool @readonly = false)
        {
            return _repo.Find(predicate, @readonly);
        }
    }
}
