﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class PalestraService : ServiceBase<Palestra>, IPalestraService
    {
        readonly IPalestraRepository _repo;
        public PalestraService(IPalestraRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
