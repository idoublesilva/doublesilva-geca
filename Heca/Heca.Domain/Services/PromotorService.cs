﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class PromotorService : ServiceBase<Promotor>, IPromotorService
    {
        readonly IPromotorRepository _repo;
        public PromotorService(IPromotorRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
