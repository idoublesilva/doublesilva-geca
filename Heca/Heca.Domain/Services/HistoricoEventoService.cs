﻿using Heca.CrossCutting.Util;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;
using Heca.Domain.Validations;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Heca.Domain.Services
{
    public class HistoricoEventoService : ServiceBase<HistoricoEvento>, IHistoricoEventoService
    {
        readonly IHistoricoEventoRepository _repo;
        readonly IEventoRepository _repoEvento;
        readonly IParticipanteRepository _repoParti;
        public HistoricoEventoService(IHistoricoEventoRepository repo, IEventoRepository repoEvento, IParticipanteRepository repoParti) : base(repo)
        {
            _repoEvento = repoEvento;
            _repo = repo;
            _repoParti = repoParti;
        }

        public override ValidationResult Add(HistoricoEvento entity)
        {
            var total = _repo.Find(x => x.IdEvento == entity.IdEvento).Count();

            if(total > 9)
            {
                var evento = _repoEvento.Find(x => x.IdEntity == entity.IdEvento).FirstOrDefault();
                if(evento.Configuracao.NroMaximoInscrito <= total)
                {
                    this._validation.Add("Número máximo de participantes atingido.");
                }
            }
            if (_validation.IsValid)
            {
                if (entity.ChaveTemporaria != null)
                {

                    entity.DtIncricao = DateTime.Now;
                    this._validation = base.Add(entity);

                }
                else
                {
                    entity.ChaveTemporaria = Guid.NewGuid();
                    this._validation = base.Add(entity);
                    if (this._validation.IsValid)
                    {
                        CultureInfo ci = new CultureInfo("pt-BR");
                        //ConfirmRegistration.html
                        var participante = _repoParti.Get(entity.IdParcipante);
                        var evento = _repoEvento.Get(entity.IdEvento);
                        string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template/ConfirmRegistration.html");

                        // Open the file to read from.
                        string readText = File.ReadAllText(path);
                        readText = readText.Replace("#NOME_PARTICIPANTE#", participante.Nome.Trim().Split(' ')[0]);
                        readText = readText.Replace("#NOME_EVENTO#", evento.Titulo);
                        readText = readText.Replace("#DATA_EVENTO#", string.Format(evento.DtEvento.Value.ToString("dd {0} MMMM {0} yyyy.", ci), "de"));
                        readText = readText.Replace("#URL_CONFIRM#", ConfigurationManager.AppSettings["UrlConfirmEvent"] + entity.ChaveTemporaria);
                        MailService mail = new MailService();
                        Task.Run(() => mail.SendEmail(participante.Email, "Confirme sua participação no evento", readText)).Wait();

                    }
                }
            }
            return this._validation;

        }
    }
}
