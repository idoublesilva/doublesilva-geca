﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class PalestranteService : ServiceBase<Palestrante>, IPalestranteService
    {
        readonly IPalestranteRepository _repo;
        public PalestranteService(IPalestranteRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
