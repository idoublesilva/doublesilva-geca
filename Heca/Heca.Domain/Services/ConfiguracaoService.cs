﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class ConfiguracaoService : ServiceBase<Configuracao>, IConfiguracaoService
    {
        private readonly IConfiguracaoRepository _repo;

        public ConfiguracaoService(IConfiguracaoRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
