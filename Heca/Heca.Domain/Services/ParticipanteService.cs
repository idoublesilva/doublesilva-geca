﻿using System;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;
using Heca.Domain.Validations;
using System.IO;
using Heca.CrossCutting.Util;
using System.Linq;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace Heca.Domain.Services
{
    public class ParticipanteService : ServiceBase<Participante>, IParticipanteService
    {
        readonly IParticipanteRepository _repo;
        public ParticipanteService(IParticipanteRepository repo) : base(repo)
        {
            _repo = repo;
        }

        public ValidationResult ReSendEmail(string guid)
        {

            try
            {
                var participante = base.Find(x => x.ChaveTemporaria.ToString().Equals(guid)).FirstOrDefault();
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template/ConfirmAccount.html");

                // Open the file to read from.
                string readText = File.ReadAllText(path);
                readText = readText.Replace("#NOME_PARTICIPANTE#", participante.Nome.Trim().Split(' ')[0]);
                readText = readText.Replace("#URL_CONFIRM#", ConfigurationManager.AppSettings["UrlConfirm"] + guid);
                MailService mail = new MailService();
                Task.Run(() => mail.SendEmail(participante.Email, "Confirmação de Cadastro Geca", readText)).Wait();
            }
            catch (Exception ex)
            {
                
                _validation.Add(ex.Message);
                _validation.Add(ex.StackTrace);
            }

            return _validation;
        }

        public override ValidationResult Add(Participante entity)
        {
            var result = base.Add(entity);    
             if(result.IsValid)
            {
                ReSendEmail(entity.ChaveTemporaria.ToString());
            }
            return result;
        }

        public ValidationResult ConfirmAccount(string guid)
        {
            var participante = _repo.Find(x => x.ChaveTemporaria.ToString() == guid).ToList().FirstOrDefault();


            if (participante == null)
                _validation.Add("Participante não encontrado.");
            else
            {
                try
                {
                    participante.ChaveTemporaria = Guid.Empty;
                   var historico = participante.HistoricoEventos.FirstOrDefault(x => x.ChaveTemporaria.ToString() == guid);
                    historico.ChaveTemporaria = Guid.Empty;
                    historico.ConfirmouInscricao = true;
                    historico.DtConfirmacao = DateTime.Now;
                    _repo.Update(participante);

                    
                }
                catch (Exception ex)
                {
                    _validation.Add(ex.Message, ex.Message, ex.StackTrace);
                    throw;
                }
            }
            return _validation;
        }
    }
}
