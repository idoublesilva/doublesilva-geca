﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class EnderecoService : ServiceBase<Endereco>, IEnderecoService
    {
        readonly IEnderecoRepository _repo;
        public EnderecoService(IEnderecoRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
