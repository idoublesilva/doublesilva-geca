﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Services.Common;

namespace Heca.Domain.Services
{
    public class AdministradorService : ServiceBase<Administrador>, IAdministradorService
    {
        readonly IAdministradorRepository _repo;
        public AdministradorService(IAdministradorRepository repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
