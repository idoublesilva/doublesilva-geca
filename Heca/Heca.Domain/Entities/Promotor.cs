﻿namespace Heca.Domain.Entities
{
    public class Promotor : Usuario
    {
        public int IdConfiguracao { get; set; }
        public virtual Configuracao ConfiguracaoEvento { get; set; }
        public int IdPermissao { get; set; }
        public virtual Permissao Permissao { get; set; }
    }
}
