﻿using System;
using System.Collections.Generic;

namespace Heca.Domain.Entities
{
    public class HistoricoEvento : BaseEntity
    {
        public HistoricoEvento()
        {
            PalestrasInscrito = new HashSet<PalestraIncrito>();
        }
        public int IdEvento { get; set; }
        public virtual Evento Evento { get; set; }

        public Guid ChaveTemporaria { get; set; }

        public int IdParcipante { get; set; }

        public virtual Participante Participante { get; set; }

        public DateTime DtIncricao { get; set; }

        public bool ConfirmouInscricao { get; set; }

        public DateTime? DtConfirmacao { get; set; }

        public bool ParticipouEvento { get; set; }

        public bool CertificadoEmitido { get; set; }

        public virtual ICollection<PalestraIncrito> PalestrasInscrito { get; set; }
    }
}