﻿namespace Heca.Domain.Entities
{
    public class Palestrante : BaseEntity
    {
        public string Nome { get; set; }

        public string Descricao { get; set; }   
    }
}