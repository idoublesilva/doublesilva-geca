﻿using System;
using System.Collections.Generic;

namespace Heca.Domain.Entities
{
    public class Configuracao : BaseEntity
    {
        public string TituloChave { get; set; }
        public bool ConfirmaEmailInscricao { get; set; }
        public bool CriarPalestra { get; set; }
        public int NroMaximoInscrito { get; set; }
        public virtual ICollection<Promotor> Promotores { get; set; }
        public DateTime? DtInicioInscricao { get; set; }
        public DateTime? DtFimInscricao { get; set; }
     

    }
}