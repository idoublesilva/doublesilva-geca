﻿using System;
using System.Collections.Generic;

namespace Heca.Domain.Entities
{
    public class Evento : BaseEntity
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public bool HabilitaEvento { get; set; }
        public bool HabilitaInscricao { get; set; }
        public int IdConfiguracao { get; set; }
        public virtual Configuracao Configuracao { get; set; }
        public int IdInfoContato { get; set; }
        public virtual InfoContato Contato { get; set; }
        public int IdEndereco { get; set; }
        public virtual Endereco Endereco { get; set; }       
        public DateTime? DtEvento { get; set; }
        public virtual ICollection<Palestra> Palestras { get; set; }

    }
}
