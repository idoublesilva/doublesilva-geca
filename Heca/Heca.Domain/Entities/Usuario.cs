﻿using System;

namespace Heca.Domain.Entities
{
    public abstract class Usuario : BaseEntity
    {
        public string Nome { get; set; }

        public string Apelido { get; set; } 
      
        public string Senha { get; set; }

        public string Email { get; set; }

        public DateTime? DtAlteracaoSenha { get; set; }

        public string UltimoSenha { get; set; }
    }
}
