﻿using System;

namespace Heca.Domain.Entities
{
    public class Palestra : BaseEntity
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Tema { get; set; }
        public DateTime DtPalestra { get; set; }
        public TimeSpan HrInicio { get; set; }
        public TimeSpan HrFim { get; set; }
        public int IdPalestrante { get; set; }
        public virtual Palestrante Palestrante { get; set; }
        public int IdLocal { get; set; }
        public virtual Local Local { get; set; }   
        public int IdEvento { get; set; }
        public virtual Evento Evento { get; set; }
       
    }
}