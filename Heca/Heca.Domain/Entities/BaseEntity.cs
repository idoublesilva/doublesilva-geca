﻿using System;

namespace Heca.Domain.Entities
{
    public class BaseEntity
    {
        public int IdEntity { get; set; }

        public DateTime? DtUpdated { get; set; }

        public DateTime DtCreated { get; set; }

        public bool IsActive { get; set; }

        public  bool IsDeleted { get; set; }
    }
}
