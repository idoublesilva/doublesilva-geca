﻿namespace Heca.Domain.Entities
{
    public class InfoContato : BaseEntity
    {
        public string Email { get; set; }

        public string Telefone { get; set; }

        public string NomeResponsavel { get; set; }
    }
}