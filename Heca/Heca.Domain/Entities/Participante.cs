﻿using System;
using System.Collections.Generic;

namespace Heca.Domain.Entities
{
    public class Participante : Usuario
    {
        public Participante()
        {
            HistoricoEventos = new HashSet<HistoricoEvento>();
        }
        public Guid ChaveTemporaria { get; set; }
        public virtual ICollection<HistoricoEvento> HistoricoEventos { get; set; }

    }
}
