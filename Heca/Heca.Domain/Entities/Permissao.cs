﻿namespace Heca.Domain.Entities
{
    public class Permissao : BaseEntity
    {
        public bool EditaEvento { get; set; }
        public bool PublicaEvento { get; set; }
        public bool HabilitaInscricao { get; set; }
        public bool GeraCertificado { get; set; }

        public bool ConfirmaPresenca { get; set; }
    }
}