﻿namespace Heca.Domain.Entities
{
    public class PalestraIncrito : BaseEntity
    {
        public int IdHistoricoEvento { get; set; }
        public virtual HistoricoEvento HistoricoEvento { get; set; }

        public int IdPalestra { get; set; }

        public virtual Palestra Palestra { get; set; }
    }
}
