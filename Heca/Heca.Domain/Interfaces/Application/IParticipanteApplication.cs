﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application.Common;
using Heca.Domain.Validations;

namespace Heca.Domain.Interfaces.Application
{
    public interface IParticipanteApplication : IApplication<Participante>
    {
        ValidationResult ReSendEmail(string guid);
        ValidationResult ConfirmAccount(string guid);
    }
}
