﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application.Common;
using Heca.Domain.Validations;

namespace Heca.Domain.Interfaces.Application
{
    public interface IEventoApplication : IApplication<Evento>
    {
        ValidationResult SendEmail(ContatoSite contato);
    }
}
