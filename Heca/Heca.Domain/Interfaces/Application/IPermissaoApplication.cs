﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application.Common;

namespace Heca.Domain.Interfaces.Application
{
    public interface IPermissaoApplication : IApplication<Permissao>
    {
    }
}
