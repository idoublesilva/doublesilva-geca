﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Repository.Common;

namespace Heca.Domain.Interfaces.Repository
{
    public interface IEventoRepository : IRepository<Evento>
    {
    }
}
