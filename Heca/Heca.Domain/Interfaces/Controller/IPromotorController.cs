﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Controller.Common;
using System.Net.Http;

namespace Heca.Domain.Interfaces.Controller
{
    public interface IPromotorController : IController<Promotor>
    {
        HttpResponseMessage ExistsUserName(string userName);
    }
}
