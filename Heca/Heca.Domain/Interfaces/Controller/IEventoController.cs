﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Controller.Common;
using System.Net.Http;

namespace Heca.Domain.Interfaces.Controller
{
    public interface IEventoController : IController<Evento>
    {
        HttpResponseMessage SendEmail(ContatoSite contato);
    }
}
