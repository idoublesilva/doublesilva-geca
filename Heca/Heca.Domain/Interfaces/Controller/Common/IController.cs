﻿using System.Net.Http;

namespace Heca.Domain.Interfaces.Controller.Common
{
    public interface IController<TEntity> where TEntity : class
    {
        HttpResponseMessage Add(TEntity entity);

        HttpResponseMessage Update(TEntity entity);

        HttpResponseMessage Delete(int id);

        HttpResponseMessage GetAll();

        HttpResponseMessage Get(int idEntity);   

        void Dispose();
    }
}
