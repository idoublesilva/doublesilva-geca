﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Controller.Common;

namespace Heca.Domain.Interfaces.Controller
{
    public interface IInfoContatoController : IController<InfoContato>
    {
    }
}
