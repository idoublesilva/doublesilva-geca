﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Service.Common;
using Heca.Domain.Validations;

namespace Heca.Domain.Interfaces.Service
{
    public interface IParticipanteService : IService<Participante>
    {
        ValidationResult ReSendEmail(string guid);
        ValidationResult ConfirmAccount(string guid);
    }
}
