﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Service.Common;
using Heca.Domain.Validations;

namespace Heca.Domain.Interfaces.Service
{
    public interface IEventoService : IService<Evento>
    {
        ValidationResult SendEmail(ContatoSite contato);
    }
}
