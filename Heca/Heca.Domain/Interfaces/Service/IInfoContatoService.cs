﻿using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Service.Common;

namespace Heca.Domain.Interfaces.Service
{
    public interface IInfoContatoService : IService<InfoContato>
    {
    }
}
