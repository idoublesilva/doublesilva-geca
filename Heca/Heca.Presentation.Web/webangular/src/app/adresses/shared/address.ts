  
  
import { BaseEntity } from "../../shared";

export class Address extends BaseEntity {
    Logradouro: string;
    Numero: string;
    Complemento: string;
    Bairro: string;
    Cep: string;
    Cidade: string;
    Estado: string;
  }