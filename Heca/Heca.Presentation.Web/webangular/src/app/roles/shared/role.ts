import { BaseEntity } from "../../shared/index";

export class Role extends BaseEntity{ 
          EditaEvento: boolean;
          PublicaEvento: boolean;
          HabilitaInscricao: boolean;
          GeraCertificado: boolean;
          ConfirmaPresenca: boolean;
}