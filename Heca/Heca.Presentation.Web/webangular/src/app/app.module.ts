import { BrowserModule } from '@angular/platform-browser';
import { CKEditorModule } from 'ng2-ckeditor';
import { MyDatePickerModule } from 'mydatepicker';
import { NgModule } from '@angular/core';
import * as moment from 'moment';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppConfig } from './app.config';
import { AppComponent } from './app.component';
import { LectureModalComponent, LectureService } from './lectures';
import { ParticipantModalComponent,ParticipantHomeComponent,  ParticipantConfirmComponent,
  ParticipantConfirmEventComponent, ParticipantService, EventHistoryService } from './participants';
import { EventComponent, EventEditComponent, EventListComponent, EventService } from './events';
import { AlertComponent, AlertService} from './alerts';
import { LoginComponent, AuthService, AuthGuard } from './auths';
import {BaseService} from './shared';
import {FormatDatePipe } from './directives/dateFormat';
import { AboutModalComponent } from './about/about.component';

//Home
import { HomeComponent, HomeBoxAuthComponent } from './home';
// Adicionamos o arquivo routing
import { routing } from './app.routing';
// Inclui nossos components
import {PromoterService, PromoterComponent, PromoterModalComponent} from './promoters';
import { BaseComponent } from "./shared";
import { ConfigurationComponent, ConfigurationService } from "./configutations";
import { DatePickerModule } from 'ng2-datepicker';
// Adicionamos em imports a contant routing e também nosso service em Providers

moment.locale('pt-br');

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlertComponent,
    AboutModalComponent,
    HomeComponent, 
    HomeBoxAuthComponent,
      LectureModalComponent,
    ConfigurationComponent,
    PromoterComponent,
    PromoterModalComponent, 
    ParticipantModalComponent,
    ParticipantHomeComponent,
    ParticipantConfirmComponent,
    ParticipantConfirmEventComponent,
     EventComponent, 
     EventEditComponent,
     EventListComponent,
     FormatDatePipe
  ],
  imports: [
    BrowserModule,
    MyDatePickerModule, 
    FormsModule,
    HttpModule,   
    DatePickerModule,    
    CKEditorModule,
    routing
  ],
  
  providers: [PromoterService, EventService, ConfigurationService, AuthService, BaseService, AuthGuard, LectureService, EventHistoryService, ParticipantService, AlertService, AppConfig],
  bootstrap: [AppComponent]
})

export class AppModule { }
