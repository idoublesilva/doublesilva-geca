import { Component, OnInit, Injector } from '@angular/core';
import {BaseComponent} from '../../shared/'
import { User } from '../../auths/index';
import { ConfigurationService, Configuration } from "../../configutations/index";
import { Router, ActivatedRoute } from "@angular/router";


declare var myLoading: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})

export class HomeComponent extends BaseComponent implements OnInit {
   
  configurations:Configuration[] = [];

   constructor(protected injector: Injector, protected router:Router, protected route:ActivatedRoute,private configService:ConfigurationService) { 
         super(injector, route, router);

    }
   
    ngOnInit(): void {
     if(this.currentUser.isAdmin){
       myLoading.show();
       this.configService.getEntities()
       .subscribe(data => {this.configurations = data.json();  myLoading.hide();});
      
     }
    }


}
