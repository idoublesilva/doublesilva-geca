import { Component, Injector } from '@angular/core';
import {User} from '../../../auths/index';
import { BaseComponent } from "../../../shared";
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: 'home-box-auth',
  templateUrl: './home.box.auth.component.html'
})
export class HomeBoxAuthComponent extends BaseComponent{
   currentUser: User;  
    constructor(protected injector: Injector, protected router:Router, protected route:ActivatedRoute) { 
        
         super(injector, route, router);
         this.route = route;
         this.router = router;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
}
