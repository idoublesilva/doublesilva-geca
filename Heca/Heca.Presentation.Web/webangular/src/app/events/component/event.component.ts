import { Component, OnInit, Injector } from '@angular/core';
import { Role } from "../../roles";
import { BaseComponent } from "../../shared";
import { PromoterService, Promoter } from "../../promoters";
import { EventService, Event } from "../../events";
import { Lecture } from "../../lectures";
import { Participant, LectureRegiter, EventHistory, EventHistoryService } from "../../participants";
import { Router, ActivatedRoute } from "@angular/router";

declare var myLoading: any;
@Component({
    selector: 'app-event',
    templateUrl: './event.component.html'
})


export class EventComponent extends BaseComponent implements OnInit {

    lectures: any[];
    contato: any = {};
    participantModal: any = { HistoricoEventos : []};
    eventModal: any;
    disableRegistration: boolean = true;
    showEvent: boolean = true;
    dataPoint: any = { NroTotal: 0, NroInscritosConfirmados: 0, NroInscritosSemConfirmacao: 0, DataNow: new Date() };
    jaInscritoEvento: boolean = false;
    constructor(protected injector: Injector, protected router: Router, protected route: ActivatedRoute,
        private eventService: EventService,
        private eventHistoryService: EventHistoryService
    ) {

        super(injector, route, router);
    }


    ngOnInit(): void {
        var id = this.route.params.subscribe(params => {
            var title = params['title'];

            if (!title)
                return;
           this.loadTitleEvent(title);
            
        });
    }

    onDoEvent(): void {
        this.eventModal = { initParticipant: true, TituloChave: this.event.Configuracao.TituloChave, currentUser: this.currentUser };
        
    }



    loadTitleEvent(title): void{
         myLoading.show();
        this.eventService.getByKeyTitle(title)
                .subscribe(data => {
                    this.event = data.json();
                    this.disableRegistration = !this.event.HabilitaInscricao && !(new Date() > new Date(this.event.Configuracao.DtInicioInscricao));
                    if (!this.event.HabilitaEvento && (!this.currentUser || this.currentUser.isParticipante)) {
                        this.showEvent = false;
                        myLoading.hide();
                        this.alertService.error('Evento ainda não está disponivel para o publico externo.', { IsAddButton: true, UrlGo: '/login', TextButton: 'Ir para Home' });
                    }
                    else {
                        if (this.event.Palestras) {
                            this.lectures = this.groupByDtPalestra(this.event.Palestras);
                        }

                        if (this.currentUser && this.currentUser.isParticipante) {
                            this.participantService.getEntity(this.currentUser.idUser).subscribe(
                                response => {
                                    this.participantModal = response.json();
                                    this.onJaInscritoEvento();
                                    myLoading.hide();
                                }
                            )
                        }
                        else if (this.currentUser && (this.currentUser.isAdmin || this.currentUser.isPromotor)) {
                            this.eventService.getDataEventPoint(this.event.IdEntity).subscribe(
                                response => {
                                    var data = response.json();
                                   
                                    if (data.length && data.length > 0) {
                                        data.forEach(x => {
                                            if (x.ConfirmouInscricao)
                                                this.dataPoint.NroInscritosConfirmados = x.NroInscrito;
                                            else this.dataPoint.NroInscritosSemConfirmacao = x.NroInscrito;
                                        });

                                        this.dataPoint.NroTotal = this.dataPoint.NroInscritosConfirmados + this.dataPoint.NroInscritosSemConfirmacao;
                                        this.dataPoint.DataNow = new Date();
                                    }
                                    myLoading.hide();
                                }
                            );
                        }
                        else myLoading.hide();
                    }
                });
    }



    onSaveParticipant(participant: Participant): void {

        myLoading.show();
        if (participant.IdEntity > 0) {
            var historyEvent = new EventHistory();
            historyEvent.IdEvento = this.event.IdEntity;
            historyEvent.IdParcipante = participant.IdEntity;
            this.eventHistoryService.addEntity(historyEvent).subscribe(
                response => {
                    var resultData = response.json();
                     myLoading.hide();
                    if (resultData.IsValid) {
                        this.alertService.success('Um email de confirmação foi enviado, confirme sua presença.');
                          this.loadTitleEvent(this.event.Configuracao.TituloChave);
                       }
                   
                }
            );
        }
        else {


            this.participantService.addEntity(participant).subscribe(
                data => {
                    var result = data.json();
                    if (result.IsValid) {
                        var historyEvent = new EventHistory();
                        historyEvent.IdEvento = this.event.IdEntity;
                        historyEvent.IdParcipante = result.Model.IdEntity;
                        historyEvent.DtIncricao = new Date().toDateString();
                        historyEvent.ChaveTemporaria = result.Model.ChaveTemporaria;
                        this.eventHistoryService.addEntity(historyEvent).subscribe(
                            response => {
                                var resultData = response.json();
                                myLoading.hide();
                                if (resultData.IsValid) {
                                    this.alertService.success('Cadastro efetuado com sucesso. Um email de confirmação foi enviado, confirme sua presença.');
                                }
                                this.loadTitleEvent(this.event.Configuracao.TituloChave);
                            }
                        );

                    }
                }
            );
        }

    }

    onJaInscrito(data: any = {}): any {
        var result = false;

        this.participantModal.HistoricoEventos.forEach(x => {
            if (x.IdEvento == this.event.IdEntity) {
                if (data.IdEntity && x.PalestrasInscrito) {
                    x.PalestrasInscrito.forEach(l => {
                        if (l.IdPalestra == data.IdEntity) {
                            //console.log('return true', data);
                            result = true;

                        }
                    });

                }
            }
        });


        return result;
    }

    onJaInscritoEvento() :void {

        if (this.currentUser && this.currentUser.isParticipante && this.participantModal) {

            this.participantModal.HistoricoEventos.forEach(x => {
              
                if (x.IdEvento == this.event.IdEntity) {
                    this.jaInscritoEvento = true;
                }
            });
        }

    }

    onConfirmLecture(data: Lecture): any {
        this.alertService.dialogResult('Deseja confirmar sua inscrição na palestra <strong>' + data.Titulo + '</strong>.');
        this.alertService.getDialogResult().subscribe(message => {
            if (message.dialogResult == 'Yes') {
                myLoading.show();

                this.participantModal.HistoricoEventos.forEach(x => {                   
                    if (x.IdEvento == this.event.IdEntity) {

                        var lectureRegiser = new LectureRegiter();
                        lectureRegiser.IdPalestra = data.IdEntity;
                        lectureRegiser.IdHistoricoEvento = x.IdEntity;
                        x.PalestrasInscrito.push(lectureRegiser);
                        this.eventHistoryService.updateEntity(x).subscribe(
                            response => {
                                var result = response.json();
                                myLoading.hide();
                                if (result.IsValid) {
                                    this.alertService.success("Palestra adicionado com sucesso.");
                                } else this.alertService.error("Não foi possivel adicionar a palestra.");


                            }
                        );

                    }


                });
            }
        });

    }
    submitted: boolean = false;
    enviarContato(IsValid: boolean) {
        this.submitted = true;
        if (IsValid) {
            this.contato.IdEvento = this.event.IdEntity;
            myLoading.show();
            this.eventService.enviarContato(this.contato).subscribe(
                response => {
                    var result = response.json();
                    myLoading.hide();
                    if (result.IsValid) {
                        this.alertService.success("Seu email foi enviado com sucesso.");

                    }
                    else this.alertService.error("Erro ao enviar email para " + result.Model.Email);
                    this.contato = {};
                    this.submitted = false;

                }
            );
        }
    }


    groupByDtPalestra(data: Lecture[]): any[] {
        // our logic to group the posts by category
        if (!data) return;

        // find out all the unique categories
        const categories = new Set(data.map(x => x.DtPalestra));

        // produce a list of category with its posts
        const result = Array.from(categories).map(x => ({
            dtPalestra: x,
            lectures: data.filter(post => post.DtPalestra === x)
        }));

        return result;
    }
}