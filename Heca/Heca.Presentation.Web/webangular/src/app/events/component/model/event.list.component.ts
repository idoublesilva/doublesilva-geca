import { Component, OnInit, Output, Injector, Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Promoter, PromoterService } from "../../../promoters";
import { Role } from "../../../roles";
import { BaseComponent } from "../../../shared";
import { Address } from "../../../adresses";
import { Lecture, LectureService } from "../../../lectures";
import { Speaker } from "../../../speakers";
import { EventService } from "../../shared/event.service";
import { Place } from "../../../places";
import { EventHistoryService } from "../../../participants";

declare var jQuery: any;
declare var myLoading: any;

@Component({
    selector: 'app-list-event',
    templateUrl: './event.list.component.html'
})

@Injectable()
export class EventListComponent extends BaseComponent {
    modelParticipants: any = {};
    disableEdit: boolean = true;
    disablePublish: boolean = true;
    disableRegistration: boolean = true;
    initRegistration: boolean = false;
    lectures: any[];
    promoter: Promoter = new Promoter();
    constructor(protected injector: Injector, protected router: Router,
        protected route: ActivatedRoute, private lectureService: LectureService,
        private promoterService: PromoterService,
        private eventService: EventService,
        private eventHistoryService: EventHistoryService
    ) {

        super(injector, route, router);
        if (this.currentUser.isAdmin || this.currentUser.isPromotor) {
            this.disablePublish = this.disableRegistration = this.disableEdit = false;
            this.route.params.subscribe(params => {
                var id = params['id'];
                this.loadData(id);
            });

        }
        else {
            router.navigateByUrl('/login');
        }
    }

    emitirCertificado(historyEvent): void {
        var mensagem = "Após habilitar o certificado não podera alterar a participação e nem desablitar a emissão do ceriticado.";

        this.alertService.dialogResult(mensagem);
        this.alertService.getDialogResult().subscribe(message => {
            if (message.dialogResult == 'Yes') {
                myLoading.show();
                historyEvent.CertificadoEmitido = !historyEvent.CertificadoEmitido;
                this.eventHistoryService.updateEntity(historyEvent).subscribe(response => {
                    var result = response.json();
                    //console.log(result);
                    if (result.IsValid) {
                        myLoading.hide();
                        this.alertService.success('Emissão de certificado habilitado com sucesso!');
                        //this.loadData(historyEvent.IdEvento);
                    }
                    else {
                        myLoading.hide();
                        this.alertService.error('Não foi possivel habilitatar a emissão de certificado.');

                    }
                });
            }
        });
    }

    confirmarParticipacao(historyEvent): void {
        var mensagem = historyEvent.ParticipouEvento ? "Deseja desconfirmar a participação?" : "Deseja confirmar a participação";
        this.alertService.dialogResult(mensagem);
        this.alertService.getDialogResult().subscribe(message => {
            if (message.dialogResult == 'Yes') {
                myLoading.show();
                historyEvent.ParticipouEvento = !historyEvent.ParticipouEvento;
                this.eventHistoryService.updateEntity(historyEvent).subscribe(response => {
                    var result = response.json();
                    //console.log(result);
                    if (result.IsValid) {
                        myLoading.hide();
                        this.alertService.success('Participação atualizada com sucesso!');
                        ///this.loadData(historyEvent.IdEvento);
                    }
                    else {
                        myLoading.hide();
                        this.alertService.error('Não foi possivel atualizar a participação.');

                    }
                });
            }
        });

    }

    removeRegister(historyEvent) {
        this.alertService.dialogResult('Após remover a inscrição o participante tera que refazer sua inscrição neste evento.');
        this.alertService.getDialogResult().subscribe(message => {
            
            if (message.dialogResult == 'Yes') {
                myLoading.show();
                if (historyEvent.IdEntity > 0) {
                    this.eventHistoryService.deleteEntity(historyEvent.IdEntity).subscribe(
                        response => {
                            
                            var result = response.json();
                            //console.log(result);
                            myLoading.hide();
                            this.loadData(historyEvent.IdEvento);
                            if (result.IsValid) {
                                this.alertService.success('Inscrição do evento removido com sucesso.');
                            }
                            else this.alertService.error('Não foi possivel remover a inscrição.');
                        });
                }
            }
        });
    }

    loadData(idEvento: number): void {
        myLoading.show();

        this.eventService.getParticipants(idEvento)
            .subscribe(data => {
                this.modelParticipants = data.json();
                //console.log(this.modelParticipants);
                myLoading.hide();
            });
    }

}