import { Component, OnInit, Output, Injector, Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Promoter, PromoterService } from "../../../promoters";
import { Role } from "../../../roles";
import { BaseComponent } from "../../../shared";
import { Address } from "../../../adresses";
import { Lecture, LectureService } from "../../../lectures";
import { Speaker } from "../../../speakers";
import { EventService } from "../../shared/event.service";
import { Place } from "../../../places";

declare var myLoading: any;
@Component({
    selector: 'app-edit-event',
    templateUrl: './event.edit.component.html'
})

@Injectable()
export class EventEditComponent extends BaseComponent {
    modelDtEvento: any;
    dataPoint: any = { NroTotal: 0, NroInscritosConfirmados: 0, NroInscritosSemConfirmacao: 0, DataNow: new Date() };
    modelEstado: any = [];
    modelCidade: any = [];
    disableEdit: boolean = true;
    disablePublish: boolean = true;
    disableRegistration: boolean = true;
    initRegistration: boolean = false;
    lectures: any[];
    promoter: Promoter = new Promoter();
    constructor(protected injector: Injector, protected router: Router,
        protected route: ActivatedRoute, private lectureService: LectureService,
        private promoterService: PromoterService,
        private eventService: EventService
    ) {

        super(injector, route, router);
        if (this.currentUser.isPromotor) {
            myLoading.show();
            this.promoterService.getEntity(this.currentUser.idUser).subscribe(data => {
                this.promoter = data.json();
                if (this.promoter.Permissao.EditaEvento) {
                    this.disableEdit = false;
                }
                myLoading.hide();
                this.loadData(this.promoter.IdConfiguracao);

            });
        }
        else if (this.currentUser.isAdmin) {
            this.disablePublish = this.disableRegistration = this.disableEdit = false;
            this.route.params.subscribe(params => {
                var id = params['id'];
                this.loadData(id);
            });

        }
        else {
            router.navigateByUrl('/login');
        }
    }
    submitted: boolean = false;
    saveEvent(isValid: boolean): void {
        this.submitted = true;

        if (isValid && this.ValidForm()) {
            myLoading.show();

            if (this.event.IdEntity > 0) {

                this.eventService.updateEntity(this.event).subscribe(
                    response => {
                        myLoading.hide();
                        var data = response.json();
                        if (data.IsValid) {
                            this.alertService.success('Evento Salvo com sucesso!!');
                            this.loadData(this.event.IdConfiguracao);
                        }
                        else this.alertService.error('Não foi possivel salvar o evento. Por favor, tente novamente.');
                        
                    }
                );
            }
            else {
                this.eventService.addEntity(this.event).subscribe(
                    response => {
                        var data = response.json();
                        //console.log(data);
                         myLoading.hide();
                        if (data.IsValid) {
                            this.alertService.success('Evento Salvo com sucesso!!');
                            this.loadData(this.event.IdConfiguracao);
                        }
                        else this.alertService.error('Não foi possivel salvar o evento. Por favor, tente novamente.');
                       
                    }
                );
            }
        }

    }

    dateValid: boolean = true;
    ValidForm(): any {
        var valid = this.dateValid = true;

        this.event.DtEvento = this.getDate(this.event.EditPickerModel.date);
        if (new Date() > this.event.DtEvento)
            valid = this.dateValid = false;
        return valid;
    }

    checkDataPoint(): void{
        myLoading.show();
          this.eventService.getDataEventPoint(this.event.IdEntity).subscribe(
                        response => {
                            var data = response.json();

                            if (data.length && data.length > 0) {
                                data.forEach(x => {
                                     //console.log(x);
                                    if (x.ConfirmouInscricao)
                                    {
                                        this.dataPoint.NroInscritosConfirmados = x.NroInscrito;
                                        this.dataPoint.DtUltimaConfirm = x.DtUltimaConfirm;
                                    }
                                    else{
                                     this.dataPoint.NroInscritosSemConfirmacao = x.NroInscrito;
                                     this.dataPoint.DtUltimaInscricao = x.DtUltimaInscricao;                                    }
                                });

                                this.dataPoint.NroTotal = this.dataPoint.NroInscritosConfirmados + this.dataPoint.NroInscritosSemConfirmacao;
                                 this.dataPoint.DataNow = new Date();
                                
                            }
                            myLoading.hide();
                        }
                    );
    }

    publishRegistration(): void {
        this.alertService.dialogResult('Após publicar o evento ele estará disponivel para o publico externo.');
        this.alertService.getDialogResult().subscribe(message => {
            if (message.dialogResult == 'Yes') {
                myLoading.show();
                this.eventService.publishEvent(this.event.IdEntity).subscribe(
                    response => {
                        var data = response.json();
                        if (data.IsValid) {
                            this.alertService.success('Evento Publicado com sucesso');
                            myLoading.hide();
                            this.loadData(this.event.Configuracao.IdEntity);
                        }
                        else {
                            this.alertService.error('Não foi possivel publicar o evento.');
                            myLoading.hide();
                        }
                    }
                );


            }

        });
    }

    enableRegistration(): void {
        this.alertService.dialogResult('Você esta liberando as inscriões ao evento antes do período previsto.');
        this.alertService.getDialogResult().subscribe(message => {
            if (message.dialogResult == 'Yes') {
                myLoading.show();
                this.eventService.enableRegistration(this.event.IdEntity).subscribe(
                    response => {
                        var data = response.json();
                        if (data.IsValid) {
                            this.alertService.success('Foram liberadas as inscrições ao evento com sucesso.');
                            myLoading.hide();
                            this.loadData(this.event.Configuracao.IdEntity);
                        }
                        else {
                            this.alertService.error('Não foi possivel habilitar inscrições ao evento.');
                            myLoading.hide();
                        }
                    }
                );


            }

        });
    }

    loadData(idConfiguracao: number): void {
        myLoading.show();

        this.eventService.getByConfiguracao(idConfiguracao)
            .subscribe(data => {
                var result = data.json();
                if (result != null) {
                    this.event = result;
                    this.initRegistration = this.event.HabilitaInscricao || new Date() > new Date(this.event.Configuracao.DtInicioInscricao);
                    this.event.EditPickerModel = this.setDateModel(this.event.DtEvento);
                    if (this.event.Palestras) {
                        this.lectures = this.groupByDtPalestra(this.event.Palestras);
                    }               

                    this.ChangeState();
                    myLoading.hide();
                   this.checkDataPoint();
                }
                else{ 
                    this.event.IdConfiguracao = idConfiguracao;
                     myLoading.hide();
                }
                
            });

            this.eventService.getEstadosCidades().subscribe(response => {
                        var result = response.json();
                        this.modelEstado = result.estados;
                    });

    }


    ChangeState(): void {
        this.modelEstado.forEach(element => {
            if (element.sigla == this.event.Endereco.Estado) {
                this.modelCidade = element.cidades;
            }

        });
    }



    public options: Object = {
        charCounterCount: false,
        toolbarButtonsSM: true,
        fullPage: false,
        toolbarButtons: ['bold', 'italic', 'underline', '|', 'fontFamily',
            'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|',
            'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote',
            'insertLink', 'insertImage', 'insertTable', '|', 'emoticons', 'clearFormatting', '|', 'html', '|', 'undo', 'redo'],
        heightMin: 250
    }

    lectureModal: any;
    onEditLecture(lecture: any): void {

        myLoading.show();
        this.lectureModal = lecture;
        this.lectureModal.EditPickerModel = this.setDateModel(this.lectureModal.DtPalestra);

        myLoading.hide();

    }

    onNewLecture(): void {
        this.lectureModal = new Lecture();
        this.lectureModal.Palestrante = new Speaker();
        this.lectureModal.Local = new Place();

    }


    onSaveLecture(lecture: any): void {

        myLoading.show();
        lecture.DtPalestra = this.getDate(this.lectureModal.EditPickerModel.date);
        if (lecture.IdEntity > 0) {

            this.lectureService.updateEntity(lecture).subscribe(
                data => {
                    var result = data.json();
                    if (result.IsValid) {
                        this.alertService.success('Palestrante salvo com sucesso.');
                        myLoading.hide();
                        this.loadData(this.event.Configuracao.IdEntity);
                    }
                    else {
                        this.alertService.error('Palestrante não salvo.');
                        myLoading.hide();
                    }


                }
            );

        }

        else {
            lecture.IdEvento = this.event.IdEntity;
            myLoading.show();

            this.lectureService.addEntity(lecture).subscribe(
                data => {
                    var result = data.json();
                    if (result.IsValid) {
                        this.alertService.success('Palestra salva com sucesso.');
                        myLoading.hide();
                        this.loadData(this.event.Configuracao.IdEntity);
                    }
                    else {
                        this.alertService.error('Não foi possivel salvar a palestra. Por favor, tente novamente.');
                        myLoading.hide();
                    }


                }
            );
        }
    }

    groupByDtPalestra(data: Lecture[]): any[] {
        // our logic to group the posts by category
        if (!data) return;

        // find out all the unique categories
        const categories = new Set(data.map(x => x.DtPalestra));

        // produce a list of category with its posts
        const result = Array.from(categories).map(x => ({
            dtPalestra: x,
            lectures: data.filter(post => post.DtPalestra === x)
        }));

        return result;
    }
}