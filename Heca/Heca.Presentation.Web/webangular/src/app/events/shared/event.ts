import { BaseEntity } from "../../shared";
import { Promoter } from "../../promoters";
import { Contact } from "../../contacts";
import { Address } from "../../adresses";
import { Lecture } from "../../lectures";

export class Event extends BaseEntity{
  Titulo: string;
  Descricao: string;
  HabilitaEvento: boolean;
  HabilitaInscricao: boolean;
  IdConfiguracao: number;
  IdInfoContato: number;
  IdEndereco: number;
  DtEvento: string;
  Promotores: Promoter[];
  Contato:Contact;
  Endereco:Address;
  Palestras: Lecture[];

}



