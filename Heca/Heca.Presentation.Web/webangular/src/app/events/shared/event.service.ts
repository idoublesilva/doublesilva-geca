import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import { AppConfig } from '../../app.config';
import { BaseService } from '../../shared/index';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class EventService extends BaseService {


    constructor(http: Http) {
        super();
        this.http = http;
        this.controller = 'evento';
    }

    // Pega uma event na API
    getByConfiguracao(idConfiguracao) {
        return this.http.get(environment.apiEndpoint + this.controller + '/getbyconfiguracao?idConfiguracao=' + idConfiguracao, this.config.authorizeHeader());
    }

    getByKeyTitle(title) {
        return this.http.get(environment.apiEndpoint + this.controller + '/getbykeytitle?title=' + title, this.config.authorizeHeader());
    }

     getDataEventPoint(idEvento) {
        return this.http.get(environment.apiEndpoint + "historicoevento" + '/getdataeventpoint?idEvento=' + idEvento, this.config.authorizeHeader());
    }

     getParticipants(idEvento) {
        return this.http.get(environment.apiEndpoint + "historicoevento" + '/getparticipants?idEvento=' + idEvento, this.config.authorizeHeader());
    }

    enviarContato(contato){
         return this.http.post(environment.apiEndpoint + this.controller + '/sendemail', contato, this.config.authorizeHeader());
    }

    // Atualiza uma event na API
    publishEvent(idEntity) {
        console.log('updateEntity' + this.controller, idEntity);
        return this.http.put(environment.apiEndpoint + this.controller + '/publishevent/'+idEntity, this.config.authorizeHeader());
    }

    // Atualiza uma event na API
    enableRegistration(idEntity) {
        console.log('enableRegistration' + this.controller, idEntity);
        return this.http.put(environment.apiEndpoint + this.controller + '/enableregistration/' + idEntity, this.config.authorizeHeader());
    }
}