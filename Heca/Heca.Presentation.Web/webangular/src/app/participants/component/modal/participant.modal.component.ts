import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Participant } from "../../shared/participant";
import { ParticipantService } from "../../shared/participant.service";
declare var myLoading: any;
declare var jQuery: any;

@Component({
    selector: 'participant-modal-selector',
    templateUrl: './participant.modal.component.html'
})
export class ParticipantModalComponent implements OnInit {
    @Input() entityParticipant: Participant;
    @Input() entityEvent: any;
    @Output() notify: EventEmitter<Participant> = new EventEmitter<Participant>();

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private participantService: ParticipantService,
    ) { }



    ngOnInit(): void {
        this.entityParticipant = new Participant();
        this.entityEvent = { initParticipant: true, TituloChave: '', currentUser: {} };

    }

     
    existsUserName:boolean = false;
    lookupDescription(name: string, isValid: boolean) {
        // initialise the description before we lookup the description
       
        if (isValid) {        
             console.log(isValid, name); 
            this.participantService.exists(name).subscribe(
                response => {                    
                    var result = response.json();
                    if(!result.IsValid)
                        this.existsUserName = true;
                    else this.existsUserName = false;
                }
            )
        }
    }


    submitted: boolean = false;
    onClick(isValid: boolean) {
        this.submitted = true;
        if (isValid) {
            jQuery("#modal_basic").modal("hide");
            this.notify.emit(this.entityParticipant);
        }
    }
}