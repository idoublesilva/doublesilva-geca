import { Component, OnInit } from '@angular/core';
import { Participant } from "../../shared/participant";
import { ParticipantService, EventHistoryService } from "../../shared/participant.service";
import { BaseComponent } from "../../../shared";
import { AlertService } from "app/alerts";
import { ActivatedRoute } from "@angular/router";
declare var myLoading: any;

@Component({
  selector: 'app-confirmparticipant',
  templateUrl: './participant.confirmevent.component.html'
})

export class ParticipantConfirmEventComponent implements OnInit {
 

       constructor(
        private eventHistoryService: EventHistoryService, private alertService : AlertService, private route: ActivatedRoute
    ) { 
        

      
    }

     ngOnInit(): void {
    this.load();
  }
   
    load(): void {
     
      this.route.params.subscribe(params => {
            var guid = params['guid'];
            myLoading.show();
            this.eventHistoryService.ConfirmEvent(guid).subscribe(
              data => {
                var result = data.json();
                if(result.IsValid){
                  this.alertService.success('Seu cadastro foi confirmado.', { IsAddButton:true, TextButton: 'Fechar', UrlGo: '/participants/dashboard'});
                }
                else {
                   this.alertService.error(result.Errors[0].MessageSystem, { IsAddButton:true, TextButton: 'Fechar', UrlGo: '/participants/dashboard'});
                }
                myLoading.hide();
              });
     
        });
    }     
    
}