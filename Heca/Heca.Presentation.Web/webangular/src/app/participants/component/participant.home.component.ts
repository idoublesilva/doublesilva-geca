import { Component, OnInit, Inject } from '@angular/core';
import { Participant, EventHistory } from "../shared/participant";
import { BaseComponent } from "../../shared";
import { ParticipantService } from "app/participants";
import * as jsPDF from 'jspdf';
import { User } from "app/auths";
import { ConvetToFile } from "app/directives/base.format";
declare var myLoading: any;

@Component({
  selector: 'app-participant-home',
  templateUrl: './participant.home.component.html'
})
export class ParticipantHomeComponent implements OnInit {
  entity: any = {};
  currentUser: User;
  dtStringEvent:string;
  constructor(private participantService: ParticipantService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }



  ngOnInit(): void {
    if (this.currentUser.isParticipante) {
      myLoading.show();
      this.participantService.getEntity(this.currentUser.idUser)
        .subscribe(data => {
          this.entity = data.json();
          console.log(this.entity);
          myLoading.hide();
    
        });

    }

  }

  getDateString(date){
    
      var options = { year: 'numeric', month: 'long', day: 'numeric' };
      return new Date(date).toLocaleString('pt-BR', options);
  }

  downloadPdf(historyEvent): void {
    var doc = new jsPDF('landscape');;


    doc.setProperties({
      title: 'GECA -  Certificado de participação',
      subject: 'Gerenciador de evento e conteúdo automatizado',
      author: 'DoubleSilva',
      keywords: historyEvent.Evento.Titulo,
      creator: 'DoubleSilva'
    });

    var img = new Image();


    img.src = './wall_1.jpg';

    ConvetToFile.convertFileToDataURLviaFileReader(img.src).subscribe(base64 => {

      //doc.addImage(base64, 'PNG', 1, 1, 295, 40);
       doc.addImage(base64, 'JPG', 0, 0, 297, 210);

        // Filled red square with black borders
        doc.setDrawColor(0);
        doc.setFillColor(245,245,245);
       doc.rect(0, 40, 297, 160, 'FD');
          
      doc.setFont("courier");
      doc.setFontType("bold");
      doc.setTextColor(255, 255, 255);
      doc.setFontSize(75);
      doc.text('GECA', 215, 20);
      doc.setFontSize(10);
       doc.text(245, 24, 'Gerenciador de evento e conteúdo automatizado', 'center');
      //doc.text(, 195, 24);
      doc.setFontSize(20);
      var nro = historyEvent.Evento.Titulo.length;
      //console.log(doc.getTextDimensions( historyEvent.Evento.Titulo));  
      //console.log(nro);
      doc.text(245, 32, historyEvent.Evento.Titulo, 'center');
      doc.setFontSize(58);
      doc.setTextColor(0);
      doc.text(148, 60, 'CERTIFICADO', 'center');
      doc.setFontSize(12);

      var dtText =this.getDateString(historyEvent.Evento.DtEvento);

      doc.text('Certifico que ' + this.entity.Nome +
        ' participou do ' + historyEvent.Evento.Titulo + ', no dia '
        + dtText + '.', 40, 80);
       
        doc.text(148, 120, historyEvent.Evento.Endereco.Cidade + ', '+ dtText + '.', 'center');
      //  doc.text(140, 40, 'GECA - Certificado de participação');
      //    doc.text(180, 80, historyEvent.Participante);
    
      doc.setLineWidth(1.5);
      doc.line(210, 160, 80, 160); // horizontal line 
      doc.addPage();
      //doc.addImage(base64, 'PNG', 1, 1, 295, 40);
       doc.addImage(base64, 'JPG', 0, 0, 297, 210);

       doc.setDrawColor(0);
       doc.setFillColor(245,245,245);
       doc.rect(0, 20, 297, 180, 'FD');

      doc.setFontSize(30);
      doc.text(148, 40, 'Palestras', 'center');

      //doc.getTextDimensions()   

      doc.save('Test.pdf');



    });

  }


  onClick() {

  }
}