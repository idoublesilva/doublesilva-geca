import { BaseEntity } from "../../shared";
import {Event } from "../../events"
export class Participant extends BaseEntity {
    ChaveTemporaria: string;
    Nome: string;
    Apelido: string;
    Senha: string;
    Email: string;
    DtAlteracaoSenha: string;
    UltimoSenha: string;
    HistoricoEventos: EventHistory[];
}

export class EventHistory extends BaseEntity{
    
        PalestrasInscrito : LectureRegiter[];
        IdEvento:number;
        Evento:Event;
        IdParcipante: number;
        DtIncricao: string;
        ConfirmouInscricao: boolean;
        DtConfirmacao: string;
        ParticipouEvento: boolean;
        CertificadoEmitido: boolean;
        ChaveTemporaria: string;

}

export class LectureRegiter extends BaseEntity{
    IdParcipante: number;
    IdPalestra: number;
    IdHistoricoEvento: number;
}