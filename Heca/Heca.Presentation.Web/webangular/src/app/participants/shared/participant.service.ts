import { Injectable, Inject } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { BaseService } from '../../shared/index';
import { Http } from "@angular/http";

@Injectable()
export class ParticipantService extends BaseService {


    constructor(http: Http) {
        super();
        this.http = http;
        this.controller = 'participante';
    }

    ReSendEmail(guid) {
        console.log('ReSendEmail' + this.controller, guid);
        return this.http.post(environment.apiEndpoint + this.controller + '/resendemail?guid=' + guid, this.config.authorizeHeader());
    }

    ConfirmAccount(guid) {
        console.log('ConfirmAccount' + this.controller, guid);
        return this.http.post(environment.apiEndpoint + this.controller + '/confirmaccount?guid=' + guid, this.config.authorizeHeader());
    }
}

@Injectable()
export class EventHistoryService extends BaseService {


    constructor(http: Http) {
        super();
        this.http = http;
        this.controller = 'historicoevento';
    }


    ConfirmEvent(guid) {
        console.log('ConfirmEvent' + this.controller, guid);
        return this.http.post(environment.apiEndpoint + this.controller + '/confirmevent?guid=' + guid, this.config.authorizeHeader());
    }

}