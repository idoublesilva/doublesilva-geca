import {Pipe, PipeTransform} from "@angular/core";
import * as moment from 'moment';

@Pipe({
    name : "datex"
})

export class FormatDatePipe implements PipeTransform{
    transform(value){
        var date_data = new Date(value);

        var yyyy = date_data.getFullYear().toString();
        var mm = (date_data.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = date_data.getDate().toString();

        return  (dd[1]?dd:"0"+dd[0]) +"/"+ (mm[1]?mm:"0"+mm[0]) +"/" + (yyyy); // returns 2016-05-16
    }
}