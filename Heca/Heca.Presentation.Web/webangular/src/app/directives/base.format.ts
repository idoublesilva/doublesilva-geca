import { Observable, Observer } from "rxjs";
import { Injectable } from "@angular/core";


@Injectable()
export class ConvetToFile {
   static convertFileToDataURLviaFileReader(url: string) :  Observable<any>
   {
        return Observable.create((observer: Observer<any>) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();
            xhr.onload = function () {
                let reader: FileReader = new FileReader();
                reader.onloadend = function () {
                    observer.next(reader.result);
                    observer.complete();
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        });
    }
}