import { Component, OnInit, Output, EventEmitter, Injector } from '@angular/core';
// Importa o Router para podermos conseguir pegar o parâmetro id
import { Router, ActivatedRoute } from '@angular/router';

// Importa nosso service para conseguirmos falar com a API
import { ConfigurationService } from '../shared/configuration.service';
import { Promoter, PromoterService } from '../../promoters';

import { Configuration } from "../shared/configuration";
import { Role } from "../../roles";
import { BaseComponent } from "../../shared";

declare var myLoading: any;
@Component({
    selector: 'app-configuration',
    templateUrl: './configuration.component.html'
})


export class ConfigurationComponent extends BaseComponent implements OnInit {

    submitted: boolean = false;
    configuration: any = {};
    promoterModal: Promoter = new Promoter();
    constructor(protected injector: Injector, protected router: Router, protected route: ActivatedRoute,
        private configurationService: ConfigurationService,
        private promoterService: PromoterService
    ) {
        super(injector, route, router);

        if (this.currentUser.isAdmin)
            this.loadData();
    }

    ngOnInit(): void {
        this.submitted = false;
    }

    loadData(): void {
        var id = this.route.params.subscribe(params => {
            var id = params['id'];

            if (!id)
                return;
            myLoading.show();
            this.configurationService.getEntity(id)
                .subscribe(data => {
                    this.configuration = data.json();
                    //console.log(this.configuration);
                    this.configuration.EditInitPickerModel = this.setDateModel(this.configuration.DtInicioInscricao);
                    this.configuration.EditEndPickerModel = this.setDateModel(this.configuration.DtFimInscricao);
                    //console.log('after', this.configuration);
                    myLoading.hide();
                });
        });
    }

    promotorModal: any;
    onEditPromoter(id: number): void {

        myLoading.show();
        var index = this.getIndexPromoter(id);
        //console.log(index);
        this.promoterModal = this.configuration.Promotores[index];
        myLoading.hide();

    }

    onSaveConfiguration(isValid: boolean): void {
        //console.log('onSaveConfiguration', isValid)
        this.submitted = true;
        this.configuration.DtInicioInscricao = this.configuration.EditInitPickerModel ? this.getDate(this.configuration.EditInitPickerModel.date) : null;
        this.configuration.DtFimInscricao = this.configuration.EditEndPickerModel ? this.getDate(this.configuration.EditEndPickerModel.date) : null;

        if (isValid && this.validInfo()) {
            myLoading.show();
            if (this.configuration.IdEntity > 0) {
                this.configurationService.updateEntity(this.configuration).subscribe(
                    data => {
                        var result = data.json();
                        if (result.IsValid) {
                            this.alertService.success('Configuração salva com sucesso.', { IsAddButton: true, UrlGo: '/admin/dashboard', TextButton: 'Ir para Home' });
                        }
                        else this.alertService.error('Configuração não salva.');
                        myLoading.hide();
                    }
                );
            }
            else {
                this.configurationService.addEntity(this.configuration).subscribe(
                    data => {
                        var result = data.json();
                        if (result.IsValid) {
                            this.alertService.success('Configuração salva com sucesso.', { IsAddButton: true, UrlGo: '/admin/dashboard', TextButton: 'Ir para Home' });
                        }
                        else this.alertService.error('Configuração não salva.');
                        myLoading.hide();
                    }
                );
            }
        }
    }

    validInfo(): any {
        var result = !this.existsSubDomain;
        var dtNow = new Date();
        if(this.configuration.NroMaximoInscrito < 10){
            this.alertService.warning('Número máximo de inscritos deve ser maior ou igual a 10.');
            result = false;
        }
        if (this.configuration.DtInicioInscricao < dtNow) {
            this.alertService.warning('Data do inicio da inscrição deve ser maior que a data atual.');
            result = false;
        }
        else if (this.configuration.DtFimInscricao < this.configuration.DtInicioInscricao) {
            this.alertService.warning('Data do fim do período da inscrição deve ser maior ou igual a data de inicio.');
            result = false;
        }
        else if (!this.configuration.Promotores || this.configuration.Promotores.length == 0) {
            this.alertService.warning('Obrigatório cadastrar ao menos um promotor.');
            result = false;
        }
         
        return result;
    }


    onNewPromoter(): void {
        this.promoterModal = new Promoter();
        this.promoterModal.Permissao = new Role();

    }

    private getIndexPromoter(idPromotorer: number): any {
        var index = 0;
        this.configuration.Promotores.forEach(function (u, i) {
            //console.log(u, 'Promotores');
            if (u.IdEntity == idPromotorer) {
                return i;
            }
        });
        return index;
    }

    onSavePromoter(promoter: Promoter): void {
        //console.log(promoter);
        if (promoter.IdEntity > 0) {
            this.configuration.Promotores[this.getIndexPromoter(promoter.IdEntity)] = promoter;
        }
        else {
            if (!this.configuration.IdEntity) {
                this.configuration.Promotores = [];
            }
            this.configuration.Promotores.push(promoter);

        }
    }
    
    existsSubDomain:boolean = false;

    lookupDescription(name: string, isValid: boolean) {
        // initialise the description before we lookup the description
       
        if (isValid) {        
             //console.log(isValid, name); 
            this.configurationService.exists(name).subscribe(
                response => {                    
                    var result = response.json();
                    if(!result.IsValid)
                        this.existsSubDomain = true;
                    else this.existsSubDomain = false;
                }
            )
        }
    }
}