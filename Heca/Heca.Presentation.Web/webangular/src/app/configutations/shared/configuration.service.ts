import { BaseService } from "../../shared/";
import { AppConfig } from "app/app.config";
import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ConfigurationService extends BaseService {

    constructor(http: Http) {
       super();
        this.http = http;    
        this.controller = 'configuracao';
    }

     exists(subDomain){         
    return this.http.get(environment.apiEndpoint+ this.controller + '/existssubdomain/?subDomain=' + subDomain, this.config.authorizeHeader());
  }
  
}