import { BaseEntity } from '../../shared';
import { Promoter } from '../../promoters'

export class Configuration extends BaseEntity {
     TituloChave: string;
    ConfirmaEmailInscricao: boolean;
    CriarPalestra: boolean;
    NroMaximoInscrito: number;
    DtInicioInscricao: string;
    DtFimInscricao: string;
    Promotores: Promoter[];
}




