import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

export class AppConfig {

     public authorizeHeader() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + currentUser.token
          });
            return new RequestOptions({ headers: headers });
        }
    }
}