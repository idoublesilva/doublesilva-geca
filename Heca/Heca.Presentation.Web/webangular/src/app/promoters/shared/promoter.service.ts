import { BaseService } from "../../shared/index";
import { AppConfig } from "app/app.config";
import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class PromoterService extends BaseService {

    constructor(http: Http) {
        super();
        this.http = http;
        this.controller = 'promotor';
    }
 
}