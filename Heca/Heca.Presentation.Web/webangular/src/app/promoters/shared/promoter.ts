import { BaseEntity } from "../../Shared/index";
import { Role } from "../../Roles/index";

export class Promoter extends BaseEntity {
      Apelido: string;     
      Email: string;
      Senha:string;
      IdConfiguracao: number;
      IdPermissao: number;
      Nome: string;
      Permissao: Role;
}