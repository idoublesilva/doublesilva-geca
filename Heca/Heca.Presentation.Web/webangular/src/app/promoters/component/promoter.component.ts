import { Component, OnInit, Output, Injector, Injectable } from '@angular/core';
// Importa nosso service para conseguirmos falar com a API
import { PromoterService } from '../shared/promoter.service';
// Importa o Router para podermos conseguir pegar o parâmetro id
import { Router, ActivatedRoute } from '@angular/router';
import { Promoter } from "../shared/promoter";
import { Role } from "../../roles";
import { BaseComponent } from "../../shared";
import { Address } from "../../adresses";
import { Lecture, LectureService } from "../../lectures";
import { Speaker } from "../../speakers";
import { EventService } from "../../events";
import { Place } from "../../places";

declare var myLoading: any;
@Component({
    selector: 'app-promoter',
    templateUrl: './promoter.component.html'
})

@Injectable()
export class PromoterComponent extends BaseComponent {
    modelDtEvento: any;
    lectures: any[];
    promoter: Promoter = new Promoter();
    constructor(protected injector: Injector, protected router: Router,
        protected route: ActivatedRoute, private lectureService: LectureService,
        private promoterService: PromoterService,
        private eventService: EventService
    ) {

        super(injector, route, router);
        if (!this.currentUser.isPromotor) {
            this.router.navigate(['login']);
        }
        this.loadData();

    }

    savePromoter(): void {

        this.event.DtEvento = this.getDate(this.event.EditPickerModel.date);
        if (this.event.IdEntity > 0) {

            this.GetResult(this.eventService.updateEntity(this.event), 'Evento Salvo com sucesso!!');
        }
        else {
            this.GetResult(this.eventService.addEntity(this.event), 'Evento Salvo com sucesso!!');
        }

    }



    loadData(): void {
        myLoading.show();
        this.promoterService.getEntity(this.currentUser.idUser)
            .subscribe(data => {
                this.promoter = data.json();
                this.eventService.getByConfiguracao(this.promoter.IdConfiguracao)
                    .subscribe(result => {

                        if (result != null) {
                            this.event = result.json();
                            this.event.EditPickerModel = this.setDateModel(this.event.DtEvento);

                            if (this.event.Palestras) {
                                this.lectures = this.groupByDtPalestra(this.event.Palestras);
                            }
                        }
                        myLoading.hide();
                    });

            });
    }


    public options: Object = {
        charCounterCount: false,
        toolbarButtonsSM: true,
        fullPage: false,
        toolbarButtons: ['bold', 'italic', 'underline', '|', 'fontFamily',
            'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|',
            'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote',
            'insertLink', 'insertImage', 'insertTable', '|', 'emoticons', 'clearFormatting', '|', 'html', '|', 'undo', 'redo'],
        heightMin: 250
    }

    lectureModal: any;
    onEditLecture(lecture: any): void {

        myLoading.show();
        this.lectureModal = lecture;
        this.lectureModal.EditPickerModel = this.setDateModel(this.lectureModal.DtPalestra);

        myLoading.hide();

    }

    onNewLecture(): void {
        this.lectureModal = new Lecture();
        this.lectureModal.Palestrante = new Speaker();
        this.lectureModal.Local = new Place();

    }


    onSaveLecture(lecture: any): void {

        myLoading.show();
        lecture.DtPalestra = this.getDate(this.lectureModal.EditPickerModel.date);
        if (lecture.IdEntity > 0) {

            this.lectureService.updateEntity(lecture).subscribe(
                data => {
                    var result = data.json();
                    if (result.IsValid) {
                        this.alertService.success('Palestrante salvo com sucesso.');
                        myLoading.hide();
                        this.loadData();
                    }
                    else {
                        this.alertService.error('Palestrante não salvo.');
                        myLoading.hide();
                    }


                }
            );

        }

        else {
            lecture.IdEvento = this.event.IdEntity;
            myLoading.show();

            this.lectureService.addEntity(lecture).subscribe(
                data => {
                    var result = data.json();
                    if (result.IsValid) {
                        this.alertService.success('Palestrante salvo com sucesso.');
                        myLoading.hide();
                        this.loadData();
                    }
                    else {
                        this.alertService.error('Palestrante não salvo.');
                        myLoading.hide();
                    }


                }
            );
        }
    }

     groupByDtPalestra(data: Lecture[]): any[] {
        // our logic to group the posts by category
        if (!data) return;

        // find out all the unique categories
        const categories = new Set(data.map(x => x.DtPalestra));

        // produce a list of category with its posts
        const result = Array.from(categories).map(x => ({
            dtPalestra: x,
            lectures: data.filter(post => post.DtPalestra === x)
        }));

        return result;
    }
}