import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { Promoter, PromoterService } from '../../../promoters';
import { Role } from '../../../roles';
declare var myLoading: any;
declare var jQuery: any;
@Component({
  selector: 'promoter-modal-selector',
  templateUrl: './promoter.modal.component.html'
})
export class PromoterModalComponent implements OnInit {
  @Input() entity: Promoter;
  @Output() notify: EventEmitter<Promoter> = new EventEmitter<Promoter>();
  submitted: boolean = false;
  
  constructor(private promoterService:PromoterService) {

  }  

  ngOnInit(): void {
    this.submitted = false;
    this.entity = new Promoter();
    this.entity.Permissao = new Role();
  }

    existsUserName:boolean = false;
    lookupDescription(name: string, isValid: boolean) {
        // initialise the description before we lookup the description
       
        if (isValid) {        
             console.log(isValid, name); 
            this.promoterService.exists(name).subscribe(
                response => {                    
                    var result = response.json();
                    if(!result.IsValid)
                        this.existsUserName = true;
                    else this.existsUserName = false;
                }
            )
        }
    }

  onClick(form: any, isValid: boolean) {
    console.log(form);
    this.submitted = true;
    if (isValid) {
      jQuery("#modal_basic").modal("hide");
      this.notify.emit(this.entity);
    }
  }
}