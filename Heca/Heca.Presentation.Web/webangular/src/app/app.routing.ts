import { ModuleWithProviders } from '@angular/core';
// Importa o modulo de rotas do Angular 2
import { Routes, RouterModule }   from '@angular/router';

// Importa seus componentes criados
import { EventComponent, EventEditComponent, EventListComponent } from './events';

import { PromoterComponent } from './promoters';
// Importa seus componentes criados
import { LoginComponent, AuthGuard } from './auths';
//Home
import { HomeComponent } from './home';
import { ConfigurationComponent } from "./configutations";
import { ParticipantConfirmComponent, ParticipantHomeComponent, ParticipantConfirmEventComponent } from "./participants";

// Cria nossas Rotas
const appRoutes: Routes = [
  { path: '', redirectTo: 'admin/dashboard', pathMatch:'full' },  
   { path: 'events/:title', component: EventComponent},
   { path: 'events/participants/:id', component: EventListComponent },
  { path: 'login', component: LoginComponent},
  { path: 'admin/dashboard', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'participants/dashboard', component: ParticipantHomeComponent, canActivate: [AuthGuard] },
  { path: 'participants/confirmaccount/:guid', component: ParticipantConfirmComponent },
  { path: 'participants/confirmevent/:guid', component: ParticipantConfirmEventComponent },
  { path: 'promoters/dashboard', component: EventEditComponent, canActivate: [AuthGuard] },
  { path: 'events/:id/edit', component: EventEditComponent, canActivate: [AuthGuard]},
  { path: 'admin/configurations/new', component: ConfigurationComponent, canActivate: [AuthGuard]},
  { path: 'admin/configurations/:id', component: ConfigurationComponent, canActivate: [AuthGuard]},
  { path: 'admin/configurations/:id/edit', component: ConfigurationComponent, canActivate: [AuthGuard]},
  // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

// Exporta a constante routing para importarmos ela no arquivo app.module.ts
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);