import { Injectable, Inject, Injector, ReflectiveInjector } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/rx';

import { environment } from '../../../environments/environment';
import { AppConfig } from '../../app.config';

interface IBaseService {

    addEntity(entity: any): any;
    updateEntity(entity: any): any;
    deleteEntity(id: number): any;
    getEntity(id: number): any;
    getEntities(): any;

}

export class BaseService implements IBaseService {
    controller: string;
    http: Http;
    config = new AppConfig();

    constructor() {

    }

    // Pega as events na API
    getEntities() {
        return this.http.get(environment.apiEndpoint + this.controller + '/getall', this.config.authorizeHeader());
    }

    // Pega uma event na API
    getEntity(id) {
        return this.http.get(environment.apiEndpoint + this.controller + '/get/' + id, this.config.authorizeHeader());
    }

    // Adiciona uma event na API
    addEntity(entity) {
        console.log('addEntity' + this.controller, entity);
        return this.http.post(environment.apiEndpoint + this.controller + '/add/', entity, this.config.authorizeHeader());
    }

    // Atualiza uma event na API
    updateEntity(entity) {
        console.log('updateEntity' + this.controller, entity);
        return this.http.put(environment.apiEndpoint + this.controller + '/update/', entity, this.config.authorizeHeader());
    }

    // Apaga uma event no servidor
    deleteEntity(id) {
        return this.http.delete(environment.apiEndpoint + this.controller + '/delete/' + id);
    }

    exists(userName) {
        return this.http.get(environment.apiEndpoint + this.controller + '/existsusername/?userName=' + userName, this.config.authorizeHeader());
    }

    getEstadosCidades(): Observable<any> {     
         return this.http.get("./estados-cidades.json");
     }

}
