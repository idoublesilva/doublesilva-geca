import { Input, Inject, Injectable, Injector, ReflectiveInjector } from "@angular/core";
import { Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from "rxjs";
import { AlertService } from "../../alerts";
import { User } from '../../auths/shared/user';
import { ParticipantService } from "../../participants";
import { Location } from '@angular/common';
declare var myLoading: any;


export class BaseComponent {

    currentUser: User;
    alertService: AlertService;
    participantService: ParticipantService;
    _location: Location;
    configCkEditor: any;   
    event: any = { Endereco:{}, Contato: {}};
    private notify = new Subject<any>();

    constructor(injector: Injector, route: ActivatedRoute, router: Router) {
        this.loadCkEditor();
        this.loadPickerConfig();
        this.alertService = injector.get(AlertService);
        this._location = injector.get(Location);

        //      this.route = Inject();//injector.get(ActivatedRoute);
        //this.router = Inject(Router);
        this.participantService = injector.get(ParticipantService);
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var returnUrl = route.snapshot.queryParams['returnUrl'] || '/';

        var path = route.snapshot.url[0].path;

        if (this.currentUser && path && path != 'events')  {
            //console.log(route.snapshot.url);
            if (this.currentUser.isAdmin && path != 'admin') { 
                if(path != 'configurations')               
                    router.navigate(['admin/dashboard']);
            }
            else if (this.currentUser.isPromotor && path != 'promoters') {
                router.navigate(['promoters/dashboard']);
            }
            else if (this.currentUser.isParticipante && path != 'participants') {
                router.navigate(['participants/dashboard']);
            }
        }
    }


    GetResult(response: Observable<Response>, messageSucesso: string): void {
        myLoading.show();
        response.subscribe(
            data => {
                var result = data.json();

                //console.log(result);

                if (result.IsValid) {

                    this.alertService.success(messageSucesso);

                }
                else this.alertService.error(result.Errors[0]);
                myLoading.hide();
                return result;
            },
            error => {
                var erro = error.json();

                //console.log(error);
                this.alertService.error(error);
                myLoading.hide();
                return error;
            });
    }

    onLogOut(): void {
        this.alertService.logOut();
    }

    backToUrl() : void{
        this._location.back();
    }

    getIndex(arrayEntity: any[], key: number): number {
        var index = 0;
        arrayEntity.forEach(function (u, i) {
            // //console.log(u, 'Promotores');
            if (u.IdEntity == key) {
                return i;
            }
        });
        return index;
    }

    loadCkEditor(): void {
        this.configCkEditor = {
            language: 'pt-br',
            toolbar: 'MyToolbar',
            toolbar_MyToolbar:
            [
                ['Source', '-', 'Templates'],
                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
                ['Undo', 'Redo'],
                ['Link', 'Unlink'],
                '/',
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['Table', 'HorizontalRule'],
                '/',
                ['Bold', 'Italic', 'Underline', 'Strike', 'Font', 'FontSize', 'NumberedList',
                    'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
                ['Maximize']
                ,
            ]
        };
    }
     configDtPicker: any;
    loadPickerConfig() : void{
        this.configDtPicker = {
            dateFormat: 'dd/mm/yyyy',
            dayLabels: {su: 'Dom', mo: 'Seg', tu: 'Ter', we: 'Qua', th: 'Qui', fr: 'Sex', sa: 'Sab'},
            todayBtnTxt: 'Hoje',
            monthLabels: { 1: 'Jan', 2: 'Fev', 3: 'Mar', 4: 'Abr', 5: 'Mai', 6: 'Jun', 7: 'Jul', 8: 'Ago', 9: 'Set', 10: 'Out', 11: 'Nov', 12: 'Dez' }
        }
    }

    setDateModel(dateSharp: any): any {

        //console.log('getDateModel', dateSharp);
        if (dateSharp) {
                if(dateSharp.date) return dateSharp; 
            var date_data = new Date(dateSharp);
            var yyyy = date_data.getFullYear().toString();
            var mm = (date_data.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = date_data.getDate().toString();

            return { date: { year: yyyy, month: mm, day: dd } };
        }

    }

    getDate(datePickerModel : any) : any{
 
             return new Date(datePickerModel.year + '/' + datePickerModel.month + '/' + datePickerModel.day);

    }

}