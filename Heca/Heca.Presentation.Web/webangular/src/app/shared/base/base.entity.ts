export class BaseEntity{
     IdEntity: number;
    DtUpdated: string;
    DtCreated: string;
    IsActive: boolean;
    IsDeleted: boolean;
}