import { Component, OnInit, Injector } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from "../shared/user";
import { BaseComponent } from "../../shared";
import { AlertService } from '../../alerts'
declare var myLoading: any;
class Credentials {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})

export class LoginComponent implements OnInit {

  returnUrl: string;
  credentials: Credentials = new Credentials();
  currentUser: User;
  constructor(private auth: AuthService,
     private alertService: AlertService,
    protected router: Router,
    protected route: ActivatedRoute
  ) {
    //super(injector, route, router);

  }

  ngOnInit() {
    // reset login status
    this.auth.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

  }


  onLogin() {

    myLoading.show();
    this.auth.login(this.credentials)
      .subscribe(
      data => {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if (this.currentUser.isAdmin) {
          this.router.navigate(['admin/dashboard']);
        }
        else if (this.currentUser.isPromotor) {

          this.router.navigate(['promoters/dashboard']);
        }
        else if (this.currentUser.isParticipante) { 
          console.log('currentUserParticipante', this.currentUser)
          if (this.currentUser.isActive) {
            if (this.returnUrl != '/') {
              this.router.navigate([this.returnUrl]);
            }
            this.router.navigate(['participants/dashboard']);
          }
          else this.alertService.warning('<h2>Usuário ainda não esta ativo. <br />Por favor, confirme o seu cadastro pelo link enviado no seu email</h2>', 
                               { IsAddButton : true, TextButton: 'Reenviar Email'})
        }
        else {
          console.log('returnUrl', this.returnUrl)

        }
        myLoading.hide();
      },
      error => {
        var erro = error.json();
        console.log(erro.error_description);
        this.alertService.error(erro.error_description);
        myLoading.hide();
      });

  }
}