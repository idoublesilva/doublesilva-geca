export class User {
    idUser: number;
    access_token : string;
    username: string;
    password: string;
    name: string;
    guid: string;
    isAdmin: boolean;
    isPromotor: boolean;
    isParticipante: boolean;
    isActive: boolean;
}