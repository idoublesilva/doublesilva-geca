import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthService {
 private controller: string = 'administrador'; 
  constructor(private http: Http) {}

  login(credentials) {
    
      let url= environment.apiEndpoint + 'security/token'
     let body = "username=" + credentials.username + "&password=" + credentials.password + "&grant_type=password";
let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
let options = new RequestOptions({ headers: headers });   
 
         
    return this.http.post(url, body, options).map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                
                if (user && user.access_token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    var aux = {                    
                        idUser: user.idUser,
    access_token : user.access_token,
    username: user.username,
    password: user.password,
    name: user.name,
    guid: user.guid,
    isAdmin: user.isAdmin == 'true',
    isPromotor: user.isPromotor == 'true',
    isParticipante: user.isParticipante == 'true',
    isActive: user.isActive == 'true'
                    };
                    localStorage.setItem('currentUser', JSON.stringify(aux));
                }
            });
  }

  logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}