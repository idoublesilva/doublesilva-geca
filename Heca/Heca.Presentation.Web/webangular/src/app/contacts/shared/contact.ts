import { BaseEntity } from "../../shared";

export class Contact extends BaseEntity {
    Email: string;
    Telefone: string;
    NomeResponsavel: string;
    
  }
