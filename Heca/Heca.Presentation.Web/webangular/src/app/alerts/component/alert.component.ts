import { Component, OnInit, OnDestroy } from '@angular/core'; 
import { Subscription } from "rxjs/Subscription";
import { Router } from "@angular/router";
import { AlertService } from '../shared/alert.service';
import { ParticipantService } from '../../participants'
declare var myLoading: any;

@Component({
    selector: 'alert',
    templateUrl: './alert.component.html'
})
 
export class AlertComponent implements OnDestroy {
    message: any; 
    subscription: Subscription;
 
    constructor(private messageService: AlertService, private participantService : ParticipantService, private router: Router) {
        // subscribe to home component messages
        this.subscription = this.messageService.getMessage().subscribe(message => { this.message = message; });
    }

    onLogOut() : void
    {
        localStorage.removeItem('currentUser');
         this.router.navigate(['login']);
    }
     
    onReenviar():void{
         var currentUser = JSON.parse(localStorage.getItem('currentUser'));
         myLoading.show();
         this.participantService.ReSendEmail(currentUser.guid).subscribe(
             data =>{
                 var result = data.json();
                 if(result.IsValid){
                         this.messageService.success
                 }
                 myLoading.hide();
                 this.message = { type: 'success', text: 'Email enviado com sucesso.' };
             }
         )
         
    }

    onClick(btnResult:string){
        this.message = null;
      this.messageService.setDialogResult(btnResult);
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }
}