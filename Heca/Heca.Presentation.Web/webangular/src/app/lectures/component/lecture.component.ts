import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Lecture } from "../../lectures";
import { Place } from "../../places";
import { Speaker } from "../../speakers";
declare var myLoading: any;
declare var jQuery: any;

@Component({
    selector: 'lecture-modal-selector',
    templateUrl: './lecture.component.html'
})
export class LectureModalComponent implements OnInit {
    @Input() entity: Lecture;
    @Output() notify: EventEmitter<Lecture> = new EventEmitter<Lecture>();

    constructor(
        private router: Router,
        private route: ActivatedRoute
    ) { }



    ngOnInit(): void {
        //throw new Error("Method not implemented.");
        this.entity = new Lecture();
        this.entity.Local = new Place();
        this.entity.Palestrante = new Speaker();
    }

    configDtPicker: any;
    getPickerConfig(): any {
        return {
            dateFormat: 'dd/mm/yyyy',
            dayLabels: { su: 'Dom', mo: 'Seg', tu: 'Ter', we: 'Qua', th: 'Qui', fr: 'Sex', sa: 'Sab' },
            todayBtnTxt: 'Hoje',
            monthLabels: { 1: 'Jan', 2: 'Fev', 3: 'Mar', 4: 'Abr', 5: 'Mai', 6: 'Jun', 7: 'Jul', 8: 'Ago', 9: 'Set', 10: 'Out', 11: 'Nov', 12: 'Dez' }
        };
    }
    
    submitted: boolean = false;
    onClick(isValid: boolean) {
        this.submitted = true;
        if (isValid) {
            jQuery("#modal_basic").modal("hide");
            this.notify.emit(this.entity);
        }
    }
}