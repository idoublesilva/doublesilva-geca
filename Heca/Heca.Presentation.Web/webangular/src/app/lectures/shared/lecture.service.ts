import { BaseService } from "../../shared/index";
import { Http } from "@angular/http";
import { Injectable } from "@angular/core";

@Injectable()
export class LectureService extends BaseService {

    constructor(http: Http) {
        super();
        this.http = http;
        this.controller = 'palestra';
    }
}