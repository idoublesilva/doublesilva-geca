import { BaseEntity } from "../../shared";
import { Speaker } from "../../speakers";
import { Place } from "../../places";
import { Event } from "../../events";

export class Lecture extends BaseEntity {
           
      Titulo: string;
      Descricao: string;
      Tema: string;
      DtPalestra: string;
      HrInicio: string;
      HrFim: string;
      IdPalestrante: number;
      IdLocal: number;
      IdEvento: number;
      Palestrante:Speaker;
      Local: Place;
      Evento: Event;
      EditPickerModel:any;
    
}




 