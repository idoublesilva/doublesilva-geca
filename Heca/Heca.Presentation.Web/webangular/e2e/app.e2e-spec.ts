import { WebangularPage } from './app.po';

describe('webangular App', () => {
  let page: WebangularPage;

  beforeEach(() => {
    page = new WebangularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
