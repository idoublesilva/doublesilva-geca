﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class ConfiguracaoApplication : ApplicationBase<Configuracao>, IConfiguracaoApplication
    {
        readonly IConfiguracaoService _service;

        public ConfiguracaoApplication(IConfiguracaoService service) : base(service)
        {
            _service = service;
        }

    }
}
