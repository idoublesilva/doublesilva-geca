﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class InfoContatoApplication : ApplicationBase<InfoContato>, IInfoContatoApplication
    {
        readonly IInfoContatoService _service;
        public InfoContatoApplication(IInfoContatoService service) : base(service)
        {
            _service = service;
        }
    }
}
