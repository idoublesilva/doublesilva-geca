﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class PalestraApplication : ApplicationBase<Palestra>, IPalestraApplication
    {
        readonly IPalestraService _service;
        public PalestraApplication(IPalestraService service) : base(service)
        {
            _service = service;
        }
    }
}
