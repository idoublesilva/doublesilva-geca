﻿using Heca.Domain.Interfaces.Application.Common;
using System;
using System.Collections.Generic;
using Heca.Domain.Validations;
using System.Linq.Expressions;
using Heca.Domain.Interfaces.Service.Common;

namespace Heca.Application.Commmon
{
    public class ApplicationBase<TEntity> : IApplication<TEntity> where TEntity : class
    {
        readonly IService<TEntity> _service;
        public ApplicationBase(IService<TEntity> service)
        {
            _service = service;
        }
        public ValidationResult Add(TEntity entity)
        {
            return _service.Add(entity);
        }

        public ValidationResult Delete(int id)
        {
            return _service.Delete(id);
        }

        public void Dispose()
        {
            _service.Dispose();            
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, bool @readonly = false)
        {
            return _service.Find(predicate, @readonly);
        }

        public TEntity Get(int idEntity)
        {
            return _service.Get(idEntity);
        }

        public IEnumerable<TEntity> GetAll(bool @readonly = false)
        {
            return _service.GetAll(@readonly);
        }

        public ValidationResult Update(TEntity entity)
        {
            return _service.Update(entity);
        }
    }
}
