﻿using System;
using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Validations;

namespace Heca.Application
{
    public class ParticipanteApplication : ApplicationBase<Participante>, IParticipanteApplication
    {
        readonly IParticipanteService _service;

        public ParticipanteApplication(IParticipanteService service) : base(service)
        {
            _service = service;
        }

        public ValidationResult ConfirmAccount(string guid)
        {
            return _service.ConfirmAccount(guid);
        }

        public ValidationResult ReSendEmail(string guid)
        {
            return _service.ReSendEmail(guid);
        }
    }
}
