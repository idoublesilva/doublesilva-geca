﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class EnderecoApplication : ApplicationBase<Endereco>, IEnderecoApplication
    {
        readonly IEnderecoService _service;
        public EnderecoApplication(IEnderecoService service) : base(service)
        {
            _service = service;
        }
    }
}
