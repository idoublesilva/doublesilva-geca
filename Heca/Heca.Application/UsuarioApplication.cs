﻿using Heca.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heca.Domain.Entities;
using System.Linq.Expressions;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class UsuarioApplication : IUsuarioApplication
    {
        private readonly IUsuarioService _service;
        public UsuarioApplication(IUsuarioService service)
        {
            _service = service;
        }
        public IEnumerable<Usuario> Find(Expression<Func<Usuario, bool>> predicate, bool @readonly = false)
        {
            return _service.Find(predicate, @readonly);
        }
    }
}
