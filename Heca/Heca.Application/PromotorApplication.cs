﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class PromotorApplication : ApplicationBase<Promotor>, IPromotorApplication
    {
        readonly IPromotorService _service;

        public PromotorApplication(IPromotorService service) : base(service)
        {
            _service = service;
        }

    }
}
