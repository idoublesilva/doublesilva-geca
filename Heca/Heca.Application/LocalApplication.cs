﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class LocalApplication : ApplicationBase<Local>, ILocalApplication
    {
        readonly ILocalService _service;
        public LocalApplication(ILocalService service) : base(service)
        {
            _service = service;
        }
    }
}
