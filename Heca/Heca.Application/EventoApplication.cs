﻿using System;
using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;
using Heca.Domain.Validations;

namespace Heca.Application
{
    public class EventoApplication : ApplicationBase<Evento>, IEventoApplication
    {
        readonly IEventoService _service;
        public EventoApplication(IEventoService service) : base(service)
        {
            _service = service;
        }

        public ValidationResult SendEmail(ContatoSite contato)
        {
            return _service.SendEmail(contato);
        }
    }
}
