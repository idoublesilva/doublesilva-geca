﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class AdministradorApplication : ApplicationBase<Administrador>, IAdministradorApplication
    {
        readonly IAdministradorService _service;
        public AdministradorApplication(IAdministradorService service) : base(service)
        {
            _service = service;
        }
    }
}
