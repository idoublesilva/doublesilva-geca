﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class PalestranteApplication : ApplicationBase<Palestrante>, IPalestranteApplication
    {
        readonly IPalestranteService _service;

        public PalestranteApplication(IPalestranteService service) : base(service)
        {
            _service = service;
        }
    }
}
