﻿using Heca.Application.Commmon;
using Heca.Domain.Entities;
using Heca.Domain.Interfaces.Application;
using Heca.Domain.Interfaces.Service;

namespace Heca.Application
{
    public class HistoricoEventoApplication : ApplicationBase<HistoricoEvento>, IHistoricoEventoApplication
    {
        readonly IHistoricoEventoService _service;
        public HistoricoEventoApplication(IHistoricoEventoService service) : base(service)
        {
            _service = service;
        }
    }
}
